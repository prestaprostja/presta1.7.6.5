<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsImageRegenerate
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosimageregenerate
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class PrestaProsImageRegenerate extends Module
{
    const PROCESS_NAME = 'products';
    const PROCESS_DIR = _PS_PROD_IMG_DIR_;
    const CONFIG_FORMAT = 'prestaprosimageregenerateformat';
    const CONFIG_IMAGE_ID_LIMIT = 'prestaprosimageregeneratelimitid';
    const CONFIG_LAST_ID = 'prestaprosimageregeneratelastid';
    const SUBMIT_SETTINGS = 'submitSettings';
    const SUBMIT_REGENERATE = 'submitRegenerate';
    const SUBMIT_RESET_LAST_ID = 'submitResetLastId';

    protected $start_time = 0;
    protected $max_execution_time = 7200;

    public function __construct()
    {
        $this->name = 'prestaprosimageregenerate';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'PrestaPros.com';
        $this->need_instance = 0;
        $this->languages = Language::getLanguages();

        $this->ps_versions_compliancy = array(
            'min' => '1.7',
            'max' => _PS_VERSION_
        );
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('PrestaPros Image Regenerate');
        $this->description = $this->l('PrestaProsImageRegenerate select process and format for regenerate images');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (!parent::install()
        ) {
            return false;
        }
        $this->setDefaultSettings();

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
        ) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        if (((bool)Tools::isSubmit(self::SUBMIT_SETTINGS)) == true) {
            $this->updateSettingField(self::CONFIG_FORMAT);
            $this->updateSettingField(self::CONFIG_IMAGE_ID_LIMIT, true);
        }
        if (((bool)Tools::isSubmit(self::SUBMIT_REGENERATE)) == true) {
            $this->regenerateImage();
        }
        if (((bool)Tools::isSubmit(self::SUBMIT_RESET_LAST_ID)) == true) {
            $this->resetLastId();
        }

        return $this->renderForm(self::SUBMIT_SETTINGS, $this->getSettingsForm(), $this->getSettingsFormValue())
            . $this->renderForm(self::SUBMIT_REGENERATE, $this->getRegenerateForm(), $this->getRegenerateFormValue())
            . $this->renderForm(self::SUBMIT_RESET_LAST_ID, $this->getResetLasIdForm(), array());
    }

    protected function resetLastId()
    {
        Configuration::updateValue(
            self::CONFIG_LAST_ID,
            '',
            false,
            null,
            $this->context->shop->id
        );
    }

    protected function getImages($lastId, $limit)
    {
        $dbquery = new DbQuery();
        $dbquery->select('i.`id_image`, i.`id_product`');
        $dbquery->from('image', 'i');
        if (!empty($lastId)) {
            $dbquery->where('i.id_image > ' . $lastId);
        }
        $dbquery->limit($limit);
        $dbquery->orderBy('i.id_image');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);
    }

    protected function getImageTotal($lastId, $rendering = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select('COUNT(i.`id_image`) AS total');
        $dbquery->from('image', 'i');
        if (!empty($lastId) && !$rendering) {
            $dbquery->where('i.id_image > ' . $lastId);
        }
        if (!empty($lastId) && $rendering) {
            $dbquery->where('i.id_image <= ' . $lastId);
        }
        $result =  Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($dbquery);

        return $result['total'];
    }

    protected function regenerateImage()
    {
        $this->start_time = time();
        ini_set('max_execution_time', $this->max_execution_time); // ini_set may be disabled, we need the real value
        $this->max_execution_time = (int) ini_get('max_execution_time');
        $lastId = Configuration::get(self::CONFIG_LAST_ID, null, null, $this->context->shop->id);
        $limit = Configuration::get(self::CONFIG_IMAGE_ID_LIMIT, null, null, $this->context->shop->id);
        if (file_exists(_PS_PROD_IMG_DIR_ . 'duplicates/')) {
            $this->context->controller->warnings[] = $this->trans(
                'Duplicate images were found when moving the product images. This is likely caused by unused'
                .' demonstration images. Please make sure that the folder %folder% only contains demonstration images,'
                .' and then delete it.',
                array('%folder%' => _PS_PROD_IMG_DIR_ . 'duplicates/'),
                'Admin.Design.Notification'
            );
        }
        $type = Configuration::get(self::CONFIG_FORMAT, null, null, $this->context->shop->id);
        $formats = ImageType::getImagesTypes(self::PROCESS_NAME);
        if ($type != 'all') {
            foreach ($formats as $k => $form) {
                if ($form['name'] != $type) {
                    unset($formats[$k]);
                }
            }
        }
        $images = $this->getImages($lastId, $limit);
        $this->deleteOldImages(self::PROCESS_DIR, $formats, $images, (self::PROCESS_NAME == 'products' ? true : false));
        if (($return = $this->regenerateNewImages(self::PROCESS_DIR, $formats, $images, (self::PROCESS_NAME == 'products' ? true : false))) === true) {
            if (!count($this->context->controller->errors)) {
                $this->context->controller->errors[] = $this->trans('Cannot write images for this type: %1$s. Please check the %2$s folder\'s writing permissions.', array(self::PROCESS_NAME, self::PROCESS_DIR), 'Admin.Design.Notification');
            }
        } elseif ($return == 'timeout') {
            $this->context->controller->errors[] = $this->trans('Only part of the images have been regenerated. The server timed out before finishing.', array(), 'Admin.Design.Notification');
        } else {
            if (self::PROCESS_NAME == 'products') {
                if ($this->regenerateWatermark(self::PROCESS_DIR, $images, $formats) == 'timeout') {
                    $this->context->controller->errors[] = $this->trans('Server timed out. The watermark may not have been applied to all images.', array(), 'Admin.Design.Notification');
                }
            }
            if (!count($this->context->controller->errors)) {
                if ($this->regenerateNoPictureImages(self::PROCESS_DIR, $formats, Language::getLanguages(false))) {
                    $this->context->controller->errors[] = $this->trans('Cannot write "No picture" image to %s images folder. Please check the folder\'s writing permissions.', array(self::PROCESS_NAME), 'Admin.Design.Notification');
                }
            }
        }

        $lastElement = end($images);
        $this->setLastImageId(
            (int)$this->getImageTotal($lastElement['id_image']) === 0 ? '' : $lastElement['id_image']
        );
    }

    protected function setLastImageId($lastId)
    {
        Configuration::updateValue(
            self::CONFIG_LAST_ID,
            $lastId,
            false,
            null,
            $this->context->shop->id
        );
    }

    protected function setDefaultSettings()
    {
        Configuration::updateValue(self::CONFIG_FORMAT, 'all');
        Configuration::updateValue(self::CONFIG_IMAGE_ID_LIMIT, 100);
        Configuration::updateValue(self::CONFIG_LAST_ID, '');
    }

    protected function updateSettingField($key, $intOnly = false)
    {
        if ($intOnly) {
            if (!empty(Tools::getValue($key)) && (int)Tools::getValue($key) > 0) {
                Configuration::updateValue(
                    $key,
                    (int)Tools::getValue($key),
                    false,
                    null,
                    $this->context->shop->id
                );
            }
        } else {
            if (!empty(Tools::getValue($key))) {
                Configuration::updateValue(
                    $key,
                    Tools::getValue($key),
                    false,
                    null,
                    $this->context->shop->id
                );
            }
        }
    }

    protected function renderForm($submit, $form, $formValue)
    {
        $helper = new HelperForm();
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = $submit;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = $formValue;

        return $helper->generateForm(array($form));
    }

    protected function getSettingsFormValue()
    {
        return array(
            'fields_value' => array(
                self::CONFIG_FORMAT => Configuration::get(
                    self::CONFIG_FORMAT,
                    null,
                    null,
                    $this->context->shop->id
                ),
                self::CONFIG_IMAGE_ID_LIMIT => Configuration::get(
                    self::CONFIG_IMAGE_ID_LIMIT,
                    null,
                    null,
                    $this->context->shop->id
                ),
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => Configuration::get('PS_LANG_DEFAULT'),
        );
    }

    protected function getSettingsForm()
    {
        $formats = $this->prepareSelectImageFormats('products');

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Regenerate Settings')
                ),
                'input' => array(
                    array(
                        'label' => $this->l('Image format'),
                        'type' => 'select',
                        'name' => self::CONFIG_FORMAT,
                        'options' => array(
                            'id' => 'id_format',
                            'name' => 'name',
                            'query' => $formats,
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Image regenerate limit, (default: 100)'),
                        'name' => self::CONFIG_IMAGE_ID_LIMIT,
                        'class' => 'input fixed-width-sm',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getResetLasIdForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Reset Last Id')
                ),
                'input' => array(
                ),
                'submit' => array(
                    'title' => $this->l('Reset'),
                ),
            ),
        );
    }

    protected function getRegenerateFormValue()
    {
        $lastId = Configuration::get(
            self::CONFIG_LAST_ID,
            null,
            null,
            $this->context->shop->id
        );

        return array(
            'fields_value' => array(
                self::CONFIG_LAST_ID => $lastId,
                'c_rendering_images' => empty($lastId) ? 0 : $this->getImageTotal($lastId, true),
                'c_all_images' => $this->getImageTotal(false),
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => Configuration::get('PS_LANG_DEFAULT'),
        );
    }

    protected function getRegenerateForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Regenerate Images')
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Last image regenerating Id'),
                        'readonly' => true,
                        'name' => self::CONFIG_LAST_ID,
                        'class' => 'input fixed-width-sm',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Count rendering images'),
                        'readonly' => true,
                        'name' => 'c_rendering_images',
                        'class' => 'input fixed-width-sm',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Count all images'),
                        'readonly' => true,
                        'name' => 'c_all_images',
                        'class' => 'input fixed-width-sm',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Regenerate'),
                ),
            ),
        );
    }

    protected function prepareSelectImageFormats($type)
    {
        $selectData = array();
        $selectData[] = array('id_format' => 'all', 'name' => $this->l('All'));
        $formats = ImageType::getImagesTypes($type);
        if (count($formats) > 0) {
            foreach ($formats as $index => $format) {
                $selectData[] = array('id_format' => $format['name'], 'name' => $format['name']);
            }
        }

        return $selectData;
    }

    protected function deleteOldImages($dir, $type, $images, $product = false)
    {
        if (!is_dir($dir)) {
            return false;
        }
        $toDel = scandir($dir, SCANDIR_SORT_NONE);

        foreach ($toDel as $d) {
            foreach ($type as $imageType) {
                if (preg_match('/^[0-9]+\-' . ($product ? '[0-9]+\-' : '') . $imageType['name'] . '\.jpg$/', $d)
                    || (count($type) > 1 && preg_match('/^[0-9]+\-[_a-zA-Z0-9-]*\.jpg$/', $d))
                    || preg_match('/^([[:lower:]]{2})\-default\-' . $imageType['name'] . '\.jpg$/', $d)) {
                    if (file_exists($dir . $d)) {
                        unlink($dir . $d);
                    }
                }
            }
        }

        // delete product images using new filesystem.
        if ($product) {
            foreach ($images as $image) {
                $imageObj = new Image($image['id_image']);
                $imageObj->id_product = $image['id_product'];
                if (file_exists($dir . $imageObj->getImgFolder())) {
                    $toDel = scandir($dir . $imageObj->getImgFolder(), SCANDIR_SORT_NONE);
                    foreach ($toDel as $d) {
                        foreach ($type as $imageType) {
                            if (preg_match('/^[0-9]+\-' . $imageType['name'] . '\.jpg$/', $d) || (count($type) > 1 && preg_match('/^[0-9]+\-[_a-zA-Z0-9-]*\.jpg$/', $d))) {
                                if (file_exists($dir . $imageObj->getImgFolder() . $d)) {
                                    unlink($dir . $imageObj->getImgFolder() . $d);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected function regenerateNewImages($dir, $type, $images, $productsImages = false)
    {
        if (!is_dir($dir)) {
            return false;
        }
        $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');

        if (!$productsImages) {
            $formated_medium = ImageType::getFormattedName('medium');
            foreach (scandir($dir, SCANDIR_SORT_NONE) as $image) {
                if (preg_match('/^[0-9]*\.jpg$/', $image)) {
                    foreach ($type as $k => $imageType) {
                        // Customizable writing dir
                        $newDir = $dir;
                        if (!file_exists($newDir)) {
                            continue;
                        }

                        if (($dir == _PS_CAT_IMG_DIR_) && ($imageType['name'] == $formated_medium) && is_file(_PS_CAT_IMG_DIR_ . str_replace('.', '_thumb.', $image))) {
                            $image = str_replace('.', '_thumb.', $image);
                        }

                        if (!file_exists($newDir . substr($image, 0, -4) . '-' . stripslashes($imageType['name']) . '.jpg')) {
                            if (!file_exists($dir . $image) || !filesize($dir . $image)) {
                                $this->context->controller->errors[] = $this->trans('Source file does not exist or is empty (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                            } elseif (!ImageManager::resize($dir . $image, $newDir . substr(str_replace('_thumb.', '.', $image), 0, -4) . '-' . stripslashes($imageType['name']) . '.jpg', (int) $imageType['width'], (int) $imageType['height'])) {
                                $this->context->controller->errors[] = $this->trans('Failed to resize image file (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($dir . $image, $newDir . substr($image, 0, -4) . '-' . stripslashes($imageType['name']) . '2x.jpg', (int) $imageType['width'] * 2, (int) $imageType['height'] * 2)) {
                                    $this->context->controller->errors[] = $this->trans('Failed to resize image file to high resolution (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                                }
                            }
                        }
                    }
                }
            }
        } else {
            foreach ($images as $image) {
                $imageObj = new Image($image['id_image']);
                $existing_img = $dir . $imageObj->getExistingImgPath() . '.jpg';
                if (file_exists($existing_img) && filesize($existing_img)) {
                    foreach ($type as $imageType) {
                        if (!file_exists($dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '.jpg')) {
                            if (!ImageManager::resize($existing_img, $dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '.jpg', (int) $imageType['width'], (int) $imageType['height'])) {
                                $this->context->controller->errors[] = $this->trans(
                                    'Original image is corrupt (%filename%) for product ID %id% or bad permission on folder.',
                                    array(
                                        '%filename%' => $existing_img,
                                        '%id%' => (int) $imageObj->id_product,
                                    ),
                                    'Admin.Design.Notification'
                                );
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($existing_img, $dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '2x.jpg', (int) $imageType['width'] * 2, (int) $imageType['height'] * 2)) {
                                    $this->context->controller->errors[] = $this->trans(
                                        'Original image is corrupt (%filename%) for product ID %id% or bad permission on folder.',
                                        array(
                                            '%filename%' => $existing_img,
                                            '%id%' => (int) $imageObj->id_product,
                                        ),
                                        'Admin.Design.Notification'
                                    );
                                }
                            }
                        }
                    }
                } else {
                    $this->context->controller->errors[] = $this->trans(
                        'Original image is missing or empty (%filename%) for product ID %id%',
                        array(
                            '%filename%' => $existing_img,
                            '%id%' => (int) $imageObj->id_product,
                        ),
                        'Admin.Design.Notification'
                    );
                }
            }
        }

        return (bool) count($this->context->controller->errors);
    }

    protected function regenerateWatermark($dir, $images, $type = null)
    {
        $result = Db::getInstance()->executeS('
		SELECT m.`name` FROM `' . _DB_PREFIX_ . 'module` m
		LEFT JOIN `' . _DB_PREFIX_ . 'hook_module` hm ON hm.`id_module` = m.`id_module`
		LEFT JOIN `' . _DB_PREFIX_ . 'hook` h ON hm.`id_hook` = h.`id_hook`
		WHERE h.`name` = \'actionWatermark\' AND m.`active` = 1');

        if ($result && count($result)) {
            foreach ($images as $image) {
                $imageObj = new Image($image['id_image']);
                if (file_exists($dir . $imageObj->getExistingImgPath() . '.jpg')) {
                    foreach ($result as $module) {
                        $moduleInstance = Module::getInstanceByName($module['name']);
                        if ($moduleInstance && is_callable(array($moduleInstance, 'hookActionWatermark'))) {
                            call_user_func(array($moduleInstance, 'hookActionWatermark'), array('id_image' => $imageObj->id, 'id_product' => $imageObj->id_product, 'image_type' => $type));
                        }

                        if (time() - $this->start_time > $this->max_execution_time - 4) { // stop 4 seconds before the tiemout, just enough time to process the end of the page on a slow server
                            return 'timeout';
                        }
                    }
                }
            }
        }
    }

    protected function regenerateNoPictureImages($dir, $type, $languages)
    {
        $errors = false;
        $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');

        foreach ($type as $image_type) {
            foreach ($languages as $language) {
                $file = $dir . $language['iso_code'] . '.jpg';
                if (!file_exists($file)) {
                    $file = _PS_PROD_IMG_DIR_ . Language::getIsoById((int) Configuration::get('PS_LANG_DEFAULT')) . '.jpg';
                }
                if (!file_exists($dir . $language['iso_code'] . '-default-' . stripslashes($image_type['name']) . '.jpg')) {
                    if (!ImageManager::resize($file, $dir . $language['iso_code'] . '-default-' . stripslashes($image_type['name']) . '.jpg', (int) $image_type['width'], (int) $image_type['height'])) {
                        $errors = true;
                    }

                    if ($generate_hight_dpi_images) {
                        if (!ImageManager::resize($file, $dir . $language['iso_code'] . '-default-' . stripslashes($image_type['name']) . '2x.jpg', (int) $image_type['width'] * 2, (int) $image_type['height'] * 2)) {
                            $errors = true;
                        }
                    }
                }
            }
        }

        return $errors;
    }
}
