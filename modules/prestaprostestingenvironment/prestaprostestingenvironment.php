<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsTestingEnvironment
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprostestingenvironment
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class PrestaProsTestingEnvironment extends Module
{
    const SUBMIT_SETTINGS = 'submitSettings';
    const CONFIG_DISPLAY = 'ENV_TEST_DISPLAY';

    public function __construct()
    {
        $this->name = 'prestaprostestingenvironment';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'PrestaPros.com';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => _PS_VERSION_
        );
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('PrestaPros testing environment');
        $this->description = $this->l('Display bars information about testing environment.');

        $this->templateFile = 'module:prestaprostestingenvironment/bars.tpl';
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('displayHeader')
        ) {
            return false;
        }
        Configuration::updateValue(self::CONFIG_DISPLAY, true);

        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function hookDisplayHeader()
    {
        if (Configuration::get(self::CONFIG_DISPLAY)) {
            $this->context->controller->addCSS(_MODULE_DIR_ . $this->name . '/views/css/bars.css', 'all');
            $this->context->smarty->assign(array(
                'logo' => _MODULE_DIR_. $this->name . '/prestapros_logo.png',
                'debug' => _PS_MODE_DEV_ ? 'ON' : 'OFF',
            ));

            return $this->display(__FILE__, 'bars.tpl');
        }
    }

    public function getContent()
    {
        if (((bool)Tools::isSubmit(self::SUBMIT_SETTINGS)) == true) {
            Configuration::updateValue(self::CONFIG_DISPLAY, Tools::getValue(self::CONFIG_DISPLAY));
        }

        return $this->renderForm();
    }

    protected function renderForm()
    {
        $helper = new HelperForm();
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = self::SUBMIT_SETTINGS;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(
                self::CONFIG_DISPLAY => Configuration::get(self::CONFIG_DISPLAY),
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => Configuration::get('PS_LANG_DEFAULT'),
        );

        return $helper->generateForm(array($this->getForm()));
    }

    protected function getForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('PrestaPros Testing Environment Settings')
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Active'),
                        'name' => self::CONFIG_DISPLAY,
                        'is_bool' => true,
                        'required' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }
}
