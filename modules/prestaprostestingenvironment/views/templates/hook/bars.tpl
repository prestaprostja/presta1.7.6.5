{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from PrestaProsTestingEnvironment
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@prestapros.com
*
* @author    PrestaPros.com
* @copyright Copyright (c) 2017-2020 PrestaPros
* @license   Commercial license
* @package   prestaprostestingenvironment
*}
<div style="background-color: grey">
    <div class="row bars-container">
        <div class="col-sm-3">
            <img src="{$logo}" alt="logo">
        </div>
        <div class="col-sm-6 bars-info">
            DEVELOPMENT ENVIRONMENT
            /
            ŚRODOWISKO TESTOWE
        </div>
        <div class="col-sm-3 bars-debug">
            DEBUG MODE: {$debug}
        </div>
    </div>
</div>