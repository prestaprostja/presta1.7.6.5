{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from MoldsConfigurators
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@prestapros.com
*
* @author    PrestaPros.com
* @copyright Copyright (c) 2017-2020 PrestaPros
* @license   Commercial license
* @package   moldsconfigurators
*}
<form id="configurator-form">
    <div class="form-wrapper">
        <div class="form-group">
            <label>Średnica wewnętrzna</label>
            <div>
                <input id="input-innerDiameter" class="configurator-input" type="number" value="{$defaultInnerDiameter}">
            </div>
        </div>
        <div class="form-group">
            <label>Wysokość grzałki</label>
            <div>
                <input id="input-heaterHeight" class="configurator-input" type="number" value="{$defaultHeaterHeight}">
            </div>
        </div>
        <div class="form-group">
            <label>Moc grzałki</label>
            <div>
                <select id="select-power" class="configurator-input">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label>Sposób wprowadzania przewodów</label>
            <div>
                <select id="select-winding-type" class="configurator-input">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label>Cena PLN</label>
            <div>
                <span id="price"></span>
            </div>
        </div>
        <div class="form-group">
            <label>Wartość PLN</label>
            <div>
                <span id="value"></span>
            </div>
        </div>
        <div class="form-group">
            <label>Średnica zewnętrzna</label>
            <div>
                <span id="outsideDiameter"></span>
            </div>
        </div>
        <div class="form-group">
            <label>Przybliżona Ilość zwojów</label>
            <div>
                <span class="maximumCoilsNumber"></span>
            </div>
        </div>
        <div class="form-group">
            <label>Model</label>
            <div>
                <span id="model"></span>
            </div>
        </div>
    </div>
</form>
<br>
<table style="border: 2px solid #0a0a0a;">
    <tr>
        <th style="border: 2px solid #0a0a0a;">
            długość całkowita
        </th>
        <th style="border: 2px solid #0a0a0a;">
            moc (W)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            długość strefy grzanej (mm)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            grubość grzałki (mm)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            średnica (mm)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            wysokość grzałki (mm)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            max. ilość zwoi
        </th>
        <th style="border: 2px solid #0a0a0a;">
            wysokość zwoi (mm)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            obwód 1 zwoju (mm)
        </th>
        <th style="border: 2px solid #0a0a0a;">
            minimalna ilość zwoi
        </th>
        <th style="border: 2px solid #0a0a0a;">
            minimalna długość strefy grzanej
        </th>
        <th style="border: 2px solid #0a0a0a;">
            maksymalna długość strefy grzanej
        </th>
        <th style="border: 2px solid #0a0a0a;">
            stosunek wysokości do średnicy wynik
        </th>
        <th style="border: 2px solid #0a0a0a;">
            min ilość zwoi opaska
        </th>
        <th style="border: 2px solid #0a0a0a;">
            min długość str grzanej opaska
        </th>
    </tr>
    <tr>
        <td id="totalLength" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="power" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="heatLength" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="heaterThickness" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="innerDiameter" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="heaterHeight" style="border: 2px solid #0a0a0a;">
        </td>
        <td class="maximumCoilsNumber" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="coilHeight" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="coilCircumference" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="minimumCoilsNumber" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="minimumHeatedZoneLength" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="maximumHeatedZoneLength" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="heightToDiameter" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="minimumCoilsNumberBand" style="border: 2px solid #0a0a0a;">
        </td>
        <td id="minimumHeatedLengthBand" style="border: 2px solid #0a0a0a;">
        </td>
    </tr>
</table>
<script type="text/javascript">
    let configuratorProduct = "true"
    let ajaxLink = "{$ajax_link}";
    let defaultInnerDiameter = "&innerDiameter={$defaultInnerDiameter}";
    let defaultHeaterHeight = "&heaterHeight={$defaultHeaterHeight}";
    let defaultPower = "&power={$defaultPower}";
    let defaultWindingType = "&windingType={$defaultWindingType}";
    let productId = "{$productId}";
</script>