$( document ).ready(function() {
    function getConfiguratorData(params) {
        $.ajax({
            url: ajaxLink + params + '&productId=' + productId,
            type: 'GET',
            dataType: 'json',
        }).done(function(data) {
            loadResponseData(data.items);
            loadWindingType(data.windingTypeOptions);
            if (!loadPowerOptions(data.powerOptions)) {
                getConfiguratorData(prepareConfigurationParams());
            }
        });
    }

    function loadResponseData(items) {
        $.each(items, function( index, item ) {
            if (item.name === 'maximumCoilsNumber') {
                $('.' + item.name).html(item.value);
            } else {
                $('#' + item.name).html(item.value);
            }
        });
    }

    function loadPowerOptions(options) {
        removePowerOptions();
        let haveSelected = false;
        $.each(options, function( index, item ) {
            let option = $('<option/>');
            option.attr({ 'value': item.value }).text(item.power);
            if (item.selected) {
                option.prop('selected', true);
                haveSelected = true;
            }
            $('#select-power').append(option);
        });
        if (!haveSelected) {
            $("#select-power").prop("selectedIndex", 0);
        }

        return haveSelected;
    }

    function loadWindingType(options) {
        removeWindingTypeOptions();
        $.each(options, function( index, item ) {
            let option = $('<option/>');
            option.attr({ 'value': item.value }).text(item.windingType);
            if (item.selected) {
                option.prop('selected', true);
            }
            $('#select-winding-type').append(option);
        });
    }

    function removePowerOptions() {
        $("#select-power option").each(function() {
            $(this).remove();
        });
    }

    function removeWindingTypeOptions() {
        $("#select-winding-type option").each(function() {
            $(this).remove();
        });
    }

    function prepareConfigurationParams() {
        return "&innerDiameter=" + $("#input-innerDiameter").val()
            + "&heaterHeight=" + $("#input-heaterHeight").val()
            + "&power=" + $("#select-power option:selected").val()
            + "&count=" + $("#quantity_wanted").val()
            + "&windingType=" + $("#select-winding-type option:selected").val();
    }

    if ((typeof(configuratorProduct) != "undefined" && configuratorProduct !== null)) {
        $(".configurator-input").change(function() {
            getConfiguratorData(prepareConfigurationParams());
        });
        $("#quantity_wanted").change(function() {
            getConfiguratorData(prepareConfigurationParams());
        });

        if((typeof(defaultInnerDiameter) != "undefined" && defaultInnerDiameter !== null)
            && (typeof(defaultHeaterHeight) != "undefined" && defaultHeaterHeight !== null)
            && (typeof(defaultPower) != "undefined" && defaultPower !== null)
            && (typeof(defaultWindingType) != "undefined" && defaultWindingType !== null)
        ) {
            getConfiguratorData(
                defaultInnerDiameter + defaultHeaterHeight + defaultPower + defaultWindingType
            );
        }
    }
});