<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

$sql = array();

$sql[] = ' CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'molds_configuration_products` (
        `id_molds_configuration_products` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `id_product` int(10) unsigned NOT NULL,
        `id_customization_field` int(10) unsigned NOT NULL,
        PRIMARY KEY (`id_molds_configuration_products`, `id_product`, `id_customization_field`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
