<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MoldsConfigurators
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the DataFeedWatch is strictly forbidden.
 * In order to obtain a license, please contact us: DataFeedWatch.com
 *
 * @author    DataFeedWatch
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   moldsconfigurators
 */

class MoldsConfiguratorsAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        if (Tools::isSubmit('action') && Tools::getValue('action') === MoldsConfigurators::AJAX_ACTION_NAME) {
            $productId = Tools::getValue('productId');
            $innerDiameter = Tools::getValue('innerDiameter');
            $heaterHeight = Tools::getValue('heaterHeight');
            $power = Tools::getValue('power');
            $count = Tools::getValue('count') ? Tools::getValue('count') : 1;
            $windingType = Tools::getValue('windingType');
            $price = $this->module->getPrice($power, $windingType, $productId);
            $this->module->updateProductCustomizedData(
                $productId,
                $price,
                $windingType,
                $heaterHeight,
                $innerDiameter,
                $power
            );
            die(
                json_encode(
                    array(
                        'items' => $this->module->prepareConfiguratorData(
                            $innerDiameter,
                            $heaterHeight,
                            $power,
                            $count,
                            $price
                        ),
                        'powerOptions' => $this->module->getPowerOptions($innerDiameter, $heaterHeight, $power),
                        'windingTypeOptions' => $this->module->getWindingTypeOptions($windingType),
                    )
                )
            );
        }
    }
}
