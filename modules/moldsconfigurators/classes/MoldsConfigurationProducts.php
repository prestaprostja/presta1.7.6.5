<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MoldsConfigurators
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   moldsconfigurators
 */


class MoldsConfigurationProducts extends ObjectModel
{
    public $id;

    public $id_product;

    public $id_customization_field;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'molds_configuration_products',
        'primary' => 'id_molds_configuration_products',
        'multilang' => false,
        'fields' => array(
            'id_product'             =>    array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'id_customization_field' =>    array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
        ),
    );

    public function __construct($id = null, $lang = null, $shop = null)
    {
        parent::__construct($id, $lang, $shop);
    }

    public static function getAllProductsIdForConfigurator()
    {
        $dbquery = new DbQuery();
        $dbquery->select('mcp.`id_molds_configuration_products`, mcp.`id_product`, mcp.`id_customization_field`');
        $dbquery->from('molds_configuration_products', 'mcp');
        $dbquery->groupBy('mcp.id_molds_configuration_products');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);
    }

    public static function getIndexByProductId($productId)
    {
        $dbquery = new DbQuery();
        $dbquery->select('mcp.`id_customization_field`');
        $dbquery->from('molds_configuration_products', 'mcp');
        $dbquery->where('mcp.`id_product` = ' . $productId);

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($dbquery);
    }
}