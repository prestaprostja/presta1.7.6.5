<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MoldsConfigurators
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   moldsconfigurators
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'moldsconfigurators/classes/MoldsConfigurationProducts.php');

class MoldsConfigurators extends Module
{
    const AJAX_ACTION_NAME = 'getConfiguratorData';
    const CUSTOMIZATION_FIELD_NAME = 'Configurator';
    const SUBMIT_PRODUCT_SELECT = 'submitProductSelectButton';
    const INNER_DIAMETER_NAME = 'Średnica wewnętrzna';
    const HEATER_HEIGHT_NAME = 'Wysokość grzałki';
    const WINDING_TYPE_NAME = 'Wyprowdzenia przewodów';
    const OUTSIDE_DIAMETER_NAME = 'Średnica zewnętrzna';
    const MAXIMUM_COILS_NUMBER_NAME = 'Ilość zwojów';
    const POWER_NAME = 'Moc grzałki';

    // Te dane poniżej można obsłużyć po stronie konfiguracji modułu i zapisywać w bazie
    const EUR_EXCHANGE_RATE = 4.6;
    const HEATER_THICKNESS = 5;
    const MINIMUM_COILS_NUMBER = 5;
    const MINIMUM_COILS_NUMBER_BAND = 3;
    const WINDING_PRICE = array(
        'A' => 110,
        'B' => 110,
        'C' => 110,
    );
    const WINDING_TYPE = array(
        'A' => 'W promieniu',
        'B' => 'W obwodzie',
        'C' => 'W osi',
    );
    const DATA = array(
        1 => array(
            'power' => 195,
            'total_length' => 340,
            'heat_length' => 250,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/340/195W 230V',
            'price_eur' => 43,
        ),
        2 => array(
            'power' => 215,
            'total_length' => 370,
            'heat_length' => 280,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/370/215W 230V',
            'price_eur' => 47,
        ),
        3 => array(
            'power' => 240,
            'total_length' => 425,
            'heat_length' => 335,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/425/240W 230V',
            'price_eur' => 50,
        ),
        4 => array(
            'power' => 295,
            'total_length' => 475,
            'heat_length' => 385,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/475/295W 230V',
            'price_eur' => 54,
        ),
        5 => array(
            'power' => 350,
            'total_length' => 550,
            'heat_length' => 460,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/610/400W 230V',
            'price_eur' => 58,
        ),
        6 => array(
            'power' => 400,
            'total_length' => 610,
            'heat_length' => 520,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/610/400W 230V',
            'price_eur' => 62,
        ),
        7 => array(
            'power' => 460,
            'total_length' => 690,
            'heat_length' => 600,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/690/460W 230V',
            'price_eur' => 64,
        ),
        8 => array(
            'power' => 610,
            'total_length' => 850,
            'heat_length' => 760,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/850/610W 230V',
            'price_eur' => 69,
        ),
        9 => array(
            'power' => 690,
            'total_length' => 990,
            'heat_length' => 900,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2X4,2/990/690W 230V',
            'price_eur' => 73,
        ),
        10 => array(
            'power' => 850,
            'total_length' => 1200,
            'heat_length' => 1110,
            'model' => 'GGRZAŁKA ZWOJOWA WRP/F/T/2,2X4,2/1200/850W',
            'price_eur' => 88,
        ),
        11 => array(
            'power' => 950,
            'total_length' => 1400,
            'heat_length' => 1310,
            'model' => 'GRZAŁKA ZWOJOWA WRP/F/T/2,2X4,2/1400/950W',
            'price_eur' => 108,
        ),
    );

    private $languages;

    public function __construct()
    {
        $this->name = 'moldsconfigurators';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'PrestaPros.com';
        $this->need_instance = 0;
        $this->languages = Language::getLanguages(false);
        $this->ps_versions_compliancy = array(
            'min' => '1.7',
            'max' => _PS_VERSION_
        );
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('4molds configurator');
        $this->description = $this->l('4molds product heater configurator');

        $this->templateFile = 'module:moldsconfigurators/configurator.tpl';
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        if (!parent::install()
            || !$this->registerHook('displayFooterProduct')
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('actionCartSave')
        ) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');
        if (!parent::uninstall()
        ) {
            return false;
        }

        return true;
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addJS(_MODULE_DIR_ . $this->name .'/views/js/configurator.js');
    }

    public function hookDisplayFooterProduct($params)
    {
        $product = $params['product'];
        if (MoldsConfigurationProducts::getIndexByProductId($product->id)) {
            $link = new Link;
            $parameters = array("action" => self::AJAX_ACTION_NAME);

            $this->context->smarty->assign(array(
                'defaultHeaterHeight' => 33,
                'defaultInnerDiameter' => 10,
                'ajax_link' => $link->getModuleLink($this->name,'ajax', $parameters),
                'defaultPower' => 195,
                'defaultWindingType' => 'A',
                'productId' => $product->id,
            ));

            return $this->display(__FILE__, 'configurator.tpl');
        }
    }

    public function hookActionCartSave()
    {
        if (!isset($this->context->cart) || !Tools::getIsset('id_product')) {
            return;
        }
        $productId = (int)Tools::getValue('id_product');
        if ($index = MoldsConfigurationProducts::getIndexByProductId($productId)) {
            if (Tools::getValue('add')) {
                $this->addToCartProductCustomization(
                    $productId,
                    Product::CUSTOMIZE_TEXTFIELD,
                    $index['id_customization_field'],
                    $this->context->cart->getProductQuantity($productId)
                );
            }

            if (Tools::getValue('update')) {
                $this->updateCustomization(
                    $productId,
                    Product::CUSTOMIZE_TEXTFIELD,
                    $index['id_customization_field'],
                    $this->context->cart->getProductQuantity($productId)
                );
            }
        }
    }

    public function getContent()
    {
        if (Tools::isSubmit(self::SUBMIT_PRODUCT_SELECT)) {
            $this->updateSelectedProducts(Tools::getValue('selected_products'));
        }

        return $this->renderForm(
            self::SUBMIT_PRODUCT_SELECT,
            array('selected_products[]' => $this->prepareProductsIdFromConfigurator()),
            $this->getProductSelectForm()
        );
    }

    public function updateProductCustomizedData(
        $productId,
        $price,
        $windingType,
        $heaterHeight,
        $innerDiameter,
        $power
    )
    {

        $this->createCartIfNotExist();
        $product = new Product($productId);
        $priceCustomization = number_format($price - $product->getPrice(false), 6, '.', '');
        $index = MoldsConfigurationProducts::getIndexByProductId($productId);
        $this->prepareProductCustomization(
            $productId,
            0,
            $index['id_customization_field'],
            Product::CUSTOMIZE_TEXTFIELD,
            $this->prepareConfigurationSettingsData($windingType, $heaterHeight, $innerDiameter, $power),
            0,
            $priceCustomization
        );
    }

    protected function prepareConfigurationSettingsData($windingType, $heaterHeight, $innerDiameter, $power)
    {
        $maximumCoilsNumber = 0;
        foreach (self::DATA as $item) {
            if ((int)$item['power'] === (int)$power) {
                $maximumCoilsNumber = $this->getMaximumCoilsNumber($item['heat_length'], $innerDiameter);
            }
        }
        $configurationData = array(
            self::WINDING_TYPE_NAME . ': ' . self::WINDING_TYPE[$windingType],
            self::HEATER_HEIGHT_NAME . ': ' . $heaterHeight,
            self::INNER_DIAMETER_NAME . ': ' . $innerDiameter,
            self::POWER_NAME . ': ' . $power,
            self::OUTSIDE_DIAMETER_NAME . ': ' . $this->getFormatNumber($innerDiameter + self::HEATER_THICKNESS),
            self::MAXIMUM_COILS_NUMBER_NAME . ': ' . $this->getFormatNumber($maximumCoilsNumber),

        );

        return implode(', ', $configurationData);
    }

    public function prepareConfiguratorData($innerDiameter, $heaterHeight, $power, $count, $price)
    {
        $coilCircumference = $this->getCoilCircumference($innerDiameter);
        $responseData = $this->prepareBasicResponseData(
            $power,
            $innerDiameter,
            $heaterHeight,
            $coilCircumference,
            $count,
            $price
        );
        foreach (self::DATA as $item) {
            if ((int)$item['power'] === (int)$power) {
                $maximumCoilsNumber = $this->getMaximumCoilsNumber($item['heat_length'], $innerDiameter);
                $responseData[] = array('name' => 'model', 'value' => $item['model']);
                $responseData[] = array('name' => 'model', 'value' => $item['model']);
                $responseData[] = array('name' => 'totalLength', 'value' => $item['total_length']);
                $responseData[] = array('name' => 'heatLength', 'value' => $item['heat_length']);
                $responseData[] = array(
                    'name' => 'maximumCoilsNumber',
                    'value' => $this->getFormatNumber($maximumCoilsNumber)
                );
                $responseData[] = array(
                    'name' => 'coilHeight',
                    'value' => $this->getFormatNumber($maximumCoilsNumber * self::HEATER_THICKNESS)
                );
            }
        }

        return $responseData;
    }

    public function createCustomizationField($productId)
    {
        $customizationField = new CustomizationField();
        $customizationField->id_product = (int)$productId;
        $customizationField->type = 1;
        $customizationField->required = 0;
        $customizationField->is_module = 1;

        foreach (Language::getLanguages(false) as $lang) {
            $customizationField->name[(int)$lang['id_lang']] = self::CUSTOMIZATION_FIELD_NAME;
        }
        $customizationField->add();

        return $customizationField->id;
    }

    public function getPowerOptions($innerDiameter, $heaterHeight, $power)
    {
        $powerOptions = array();
        $coilCircumference = $this->getCoilCircumference($innerDiameter);
        foreach (self::DATA as $item) {
            $heightToDiameter = $this->getHeightToDiameter($innerDiameter, $heaterHeight, $coilCircumference);
            $maximumHeatedZoneLength = ($heaterHeight / self::HEATER_THICKNESS) * $coilCircumference;
            if ($item['heat_length'] >= $heightToDiameter && $item['heat_length'] <= $maximumHeatedZoneLength) {
                $powerOptions[] = (int)$power === (int)$item['power'] ?
                    array('power' => $item['power'] . 'W', 'value' => $item['power'], 'selected' => true) :
                    array('power' => $item['power'] . 'W', 'value' => $item['power'], 'selected' => false);
            }
        }

        return $powerOptions;
    }

    public function getWindingTypeOptions($windingType)
    {
        $windingTypeOptions = array();
        foreach (self::WINDING_TYPE as $key => $item) {
            $windingTypeOptions[] = (string)$key === (string)$windingType ?
                array('windingType' => $item, 'value' => $key, 'selected' => true) :
                array('windingType' => $item, 'value' => $key, 'selected' => false);
        }

        return $windingTypeOptions;
    }

    public function getPrice($power, $windingType, $productId)
    {
        $priceEUR = 0;
        $product = new Product($productId);
        foreach (self::DATA as $item) {
            if ((int)$item['power'] === (int)$power) {
                $priceEUR = $item['price_eur'] - $product->price;
            }
        }

        return $priceEUR * self::EUR_EXCHANGE_RATE + self::WINDING_PRICE[$windingType];
    }

    protected function createCartIfNotExist()
    {
        if (!$this->context->cart->id) {
            $cart = new Cart();
            $cart->id_currency = $this->context->currency->id;
            $cart->id_lang = $this->context->language->id;
            $cart->id_shop_group = $this->context->shop->id_shop_group;
            $cart->save();
            $this->context->cart = $cart;
            $this->context->cookie->id_cart = $cart->id;
        }
    }

    protected function prepareProductsIdFromConfigurator()
    {
        $configuratorData = array();
        $moldsConfiguratorProducts = MoldsConfigurationProducts::getAllProductsIdForConfigurator();
        if (count($moldsConfiguratorProducts) > 0) {
            foreach ($moldsConfiguratorProducts as $moldsConfiguratorProduct) {
                $configuratorData[] = $moldsConfiguratorProduct['id_product'];
            }
        }

        return $configuratorData;
    }

    protected function updateSelectedProducts($selectedProductsId)
    {
        $moldsConfiguratorProducts = MoldsConfigurationProducts::getAllProductsIdForConfigurator();
        if (is_array($moldsConfiguratorProducts) && count($moldsConfiguratorProducts) > 0) {
            foreach ($moldsConfiguratorProducts as $moldsConfiguratorProduct) {
                if (is_array($selectedProductsId)
                    && $index = array_search($moldsConfiguratorProduct['id_product'], $selectedProductsId)
                ) {
                    unset($selectedProductsId[$index]);
                    continue;
                }
                $this->removeMoldsConfigurationProduct($moldsConfiguratorProduct);
            }

        }
        if (is_array($selectedProductsId) && count($selectedProductsId) > 0) {
            foreach ($selectedProductsId as $selectedProductId) {
                $product = new Product($selectedProductId);
                $customizationFiledId = $this->checkProductHaveConfiguratorCustomizationField(
                    $product->getCustomizationFields($this->context->language->id)
                );
                if (!$customizationFiledId) {
                    $customizationFiledId = $this->createCustomizationField($product->id);
                }
                $this->addMoldsConfigurationProduct($product->id, $customizationFiledId);
            }
        }
    }

    protected function addMoldsConfigurationProduct($productId, $customizationFiledId)
    {
        $moldConfigurationProduct = new MoldsConfigurationProducts();
        $moldConfigurationProduct->id_product = $productId;
        $moldConfigurationProduct->id_customization_field = $customizationFiledId;
        $moldConfigurationProduct->save();
    }

    protected function removeMoldsConfigurationProduct($moldsConfiguratorProduct)
    {
        $customizationFiled = new CustomizationField($moldsConfiguratorProduct['id_customization_field']);
        $customizationFiled->delete();

        $moldConfigurationProduct = new MoldsConfigurationProducts(
            $moldsConfiguratorProduct['id_molds_configuration_products']
        );
        $moldConfigurationProduct->delete();
    }

    protected function checkProductHaveConfiguratorCustomizationField($productCustomizationFields)
    {
        if (is_array($productCustomizationFields) && count($productCustomizationFields) > 0) {
            foreach ($productCustomizationFields as $productCustomizationField) {
                if (self::CUSTOMIZATION_FIELD_NAME === $productCustomizationField['name']) {
                    return $productCustomizationField['id_customization_field'];
                }
            }
        }

        return false;
    }

    protected function renderForm($submitAction, $fieldValues, $form)
    {
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->default_form_language = $lang->id;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = $submitAction;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name
            .'&tab_module='.$this->tab
            .'&module_name='.$this->name;

        $helper->tpl_vars = array(
            'fields_value' => $fieldValues,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($form));
    }

    protected function getProductSelectForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Configurator Products')
                ),
                'input' => array(
                    array(
                        'col' => 6,
                        'type' => 'select',
                        'name' => 'selected_products[]',
                        'label' => $this->l('Select products for configurator'),
                        'class' => 'chosen',
                        'multiple' => true,
                        'options' => array(
                            'query' => $this->getProductsToSelect(),
                            'id' => 'id_product',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function updateCustomization($productId, $type, $index, $quantity)
    {
        $existingCustomization = $this->getExistingCustomization($productId);
        foreach ($existingCustomization as $customization) {
            if ($customization['type'] == $type && $customization['index'] == $index) {
                $this->updateCustomizationQuantity($quantity, $customization['id_customization']);
            }
        }
    }

    protected function prepareProductCustomization(
        $productId,
        $productAttributeId,
        $index,
        $type,
        $field,
        $quantity,
        $price
    )
    {
        $existingCustomization = $this->getExistingCustomization($productId, 0);
        $customizationId = false;
        if (count($existingCustomization) > 0) {
            foreach ($existingCustomization as $customization) {
                if ($customization['type'] == $type && $customization['index'] == $index) {
                    $customizationId = $customization['id_customization'];
                }
            }
        }
        $customizationId = $customizationId ?
            $customizationId :
            $this->addCustomization($productId, $productAttributeId, $quantity, 0);

        return $this->addCustomizedData($customizationId, $type, $price, $index, $field);
    }

    protected function addToCartProductCustomization($productId, $type, $index, $quantity)
    {
        $existingCustomization = $this->getExistingCustomization($productId, 0);
        if (count($existingCustomization) > 0) {
            foreach ($existingCustomization as $customization) {
                if ($customization['type'] == $type && $customization['index'] == $index) {
                    Db::getInstance()->execute(
                        'UPDATE `'._DB_PREFIX_.'customization` SET `quantity` =  ' . (int)$quantity .
                        ', `in_cart` = ' . 1 . ' WHERE `id_customization` = '. (int)$customization['id_customization']
                    );
                    Db::getInstance()->execute(
                        'UPDATE `'._DB_PREFIX_.'cart_product` SET `id_customization` =  '. (int)$customization['id_customization']
                        .' WHERE id_cart = '. (int)$this->context->cart->id . ' AND id_product = ' . (int)$productId. ' AND id_customization = 0'
                    );
                }
            }
        }
    }

    protected function prepareBasicResponseData(
        $power,
        $innerDiameter,
        $heaterHeight,
        $coilCircumference,
        $count,
        $price
    )
    {
        $price = ($price + ($price / 100 * 23));
        $heightToDiameter = $this->getHeightToDiameter($innerDiameter, $heaterHeight, $coilCircumference);
        $maximumHeatedZoneLength = ($heaterHeight / self::HEATER_THICKNESS) * $coilCircumference;

        $responseData = array();
        $responseData[] = array('name' => 'power', 'value' => $power);
        $responseData[] = array('name' => 'count', 'value' => $count);
        $responseData[] = array('name' => 'innerDiameter', 'value' => $innerDiameter);
        $responseData[] = array('name' => 'heaterHeight', 'value' => $heaterHeight);
        $responseData[] = array('name' => 'coilCircumference', 'value' => $this->getFormatNumber($coilCircumference));
        $responseData[] = array('name' => 'heaterThickness', 'value' => self::HEATER_THICKNESS);
        $responseData[] = array('name' => 'minimumCoilsNumber', 'value' => self::MINIMUM_COILS_NUMBER);
        $responseData[] = array('name' => 'minimumCoilsNumberBand', 'value' => self::MINIMUM_COILS_NUMBER_BAND);
        $responseData[] = array(
            'name' => 'minimumHeatedZoneLength',
            'value' => $this->getFormatNumber($coilCircumference * self::MINIMUM_COILS_NUMBER)
        );
        $responseData[] = array(
            'name' => 'maximumHeatedZoneLength',
            'value' => $this->getFormatNumber($maximumHeatedZoneLength)
        );
        $responseData[] = array(
            'name' => 'minimumHeatedLengthBand',
            'value' => $this->getFormatNumber($coilCircumference * self::MINIMUM_COILS_NUMBER_BAND)
        );
        $responseData[] = array('name' => 'heightToDiameter',
            'value' => $this->getFormatNumber($heightToDiameter)
        );
        $responseData[] = array(
            'name' => 'outsideDiameter',
            'value' => $this->getFormatNumber($innerDiameter + self::HEATER_THICKNESS)
        );
        $responseData[] = array(
            'name' => 'price',
            'value' => $this->getFormatNumber($price)
        );
        $responseData[] = array(
            'name' => 'value',
            'value' => $this->getFormatNumber($price * $count)
        );

        return $responseData;
    }

    protected function getFormatNumber($number)
    {
        return number_format($number, 2, '.', '');
    }

    protected function getMaximumCoilsNumber($heatLength, $innerDiameter)
    {
        return $heatLength / (2 * 3.14 * (0.5 * $innerDiameter + 1.1));
    }

    protected function getCoilCircumference($innerDiameter)
    {
        return 2 * 3.14 * (0.5 * $innerDiameter + 1.1);
    }

    protected function getHeightToDiameter($innerDiameter, $heaterHeight, $coilCircumference)
    {
        return $heaterHeight <= $innerDiameter ?
            $coilCircumference * self::MINIMUM_COILS_NUMBER_BAND :
            $coilCircumference * self::MINIMUM_COILS_NUMBER;
    }

    protected function updateCustomizationQuantity($quantity, $customizationId)
    {
        Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'customization` SET quantity = ' . (int)$quantity
            . ' WHERE id_customization = ' . (int)$customizationId
        );
    }

    protected function addCustomization($productId, $productAttributeId, $quantity, $inCart)
    {
        Db::getInstance()->execute('
            INSERT INTO `'._DB_PREFIX_.'customization` (`id_cart`, `id_product`, `id_product_attribute`, `quantity`, `in_cart`)
            VALUES ('.(int)$this->context->cart->id.', '.(int)$productId.', '.(int)$productAttributeId.', '.(int)$quantity.', ' . $inCart . ')'
        );

        return Db::getInstance()->Insert_ID();
    }

    protected function addCustomizedData($customizationId, $type, $price, $index, $field)
    {
        return $this->checkCustomizedDataExist($customizationId, $index) ?
            $this->updateCustomizedData($customizationId, $price, $index, $field) :
            $this->createCustomizedData($customizationId, $type, $price, $index, $field);
    }

    protected function createCustomizedData($customizationId, $type, $price, $index, $field)
    {
        $query = 'INSERT INTO `'._DB_PREFIX_.'customized_data` (`id_customization`, `type`, `price`, `index`, `value`)
            VALUES ('.(int)$customizationId.', '.(int)$type.', '.$price.', '.(int)$index.', \''.pSQL($field).'\')';

        return !Db::getInstance()->execute($query) ? false : $customizationId;
    }

    protected function updateCustomizedData($customizationId, $price, $index, $field)
    {
        $query = 'UPDATE `'._DB_PREFIX_.'customized_data` SET `price` = ' . $price . ', `value` = \''
            .pSQL($field).'\' WHERE `id_customization` = ' . (int)$customizationId . ' AND `index` = ' . $index;

        return !Db::getInstance()->execute($query) ? false : $customizationId;
    }

    protected function checkCustomizedDataExist($customizationId, $index)
    {
        $dbquery = new DbQuery();
        $dbquery->select('cd.`id_customization`');
        $dbquery->from('customized_data', 'cd');
        $dbquery->where('cd.index = ' . (int)$index);
        $dbquery->where('cd.id_customization = ' . (int)$customizationId);

        return count(DB::getInstance()->executeS($dbquery)) > 0;
    }

    protected function getExistingCustomization($productId, $inCart = 0)
    {
        $dbquery = new DbQuery();
        $dbquery->select('cu.`id_customization`, cd.`index`, cd.`value`, cd.`type`');
        $dbquery->from('customization', 'cu');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'customized_data` cd ON cd.`id_customization` = cu.`id_customization`');
        $dbquery->where('cu.id_cart = ' . (int)$this->context->cart->id);
        $dbquery->where('cu.id_product = ' . (int)$productId);
        $dbquery->where('cu.in_cart = ' . $inCart);

        return DB::getInstance()->executeS($dbquery);
    }

    protected function getProductsToSelect()
    {
        $dbquery = new DbQuery();
        $dbquery->select('p.`id_product`, pl.`name`');
        $dbquery->from('product', 'p');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pl.`id_product` = p.`id_product`');
        $dbquery->where('pl.id_lang = ' . (int)$this->context->language->id);
        $dbquery->groupBy('p.`id_product`');

        return DB::getInstance()->executeS($dbquery);
    }
}