<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

class BlogPostImage extends ObjectModel
{
    public $id;

    public $id_blog_post;

    public $image_name;

    public $image_name_with_ext;

    public $image_card_name_with_ext;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'blog_post_image',
        'primary' => 'id_blog_post_image',
        'fields' => array(
            'id_blog_post' =>           array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'image_name' =>             array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString',
                'size' => 64,
                'required' => true
            ),
            'image_name_with_ext' =>    array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString',
                'size' => 128,
                'required' => true
            ),
            'image_card_name_with_ext' =>    array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString',
                'size' => 128,
                'required' => true
            )
        ),
    );

    public function __construct($id = null, $lang = null, $shop = null)
    {
        parent::__construct($id, $lang, $shop);
    }

    public static function getImageByPostId($postId)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bpi.`id_blog_post_image`, bpi.`image_name_with_ext`, bpi.`image_card_name_with_ext`');
        $dbquery->from('blog_post_image', 'bpi');
        $dbquery->where('bpi.id_blog_post = ' . $postId);

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($dbquery);
    }
}
