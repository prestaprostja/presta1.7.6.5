<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

class BlogCategory extends ObjectModel
{
    public $id;

    public $id_parent_category;

    public $position;

    public $title;

    public $description;

    public $meta_title;

    public $meta_description;

    public $seo_url;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'blog_category',
        'primary' => 'id_blog_category',
        'multilang' => true,
        'fields' => array(
            'id_parent_category' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'position' =>           array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            // Lang fields
            'title' =>              array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'required' => true,
                'size' => 64
            ),
            'description' =>        array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'size' => 128
            ),
            'meta_title' =>         array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'size' => 64
            ),
            'meta_description' =>   array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'size' => 64
            ),
            'seo_url' =>            array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'required' => true,
                'size' => 64
            ),
        ),
    );

    public function __construct($id = null, $lang = null, $shop = null)
    {
        Shop::addTableAssociation('blog_category', array('type' => 'shop'));
        parent::__construct($id, $lang, $shop);
    }

    public function delete()
    {
        if (self::countCategoryHasPosts($this->id) > 0) {
            throw new PrestaShopException('A category with a posts cannot be deleted');
        }
        if (count(self::getChildCategories($this->id)) > 0) {
            throw new PrestaShopException('A category with a child category cannot be deleted');
        }
        if (1 >= self::countBlogCategory()) {
            throw new PrestaShopException('The last category cannot be deleted');
        }

        return parent::delete();
    }

    public function add($auto_date = true, $null_values = false)
    {
        if (!$this->checkSeoUrlUnique($this->seo_url)) {
            throw new PrestaShopException('Friendly url field must be unique');
        }
        if ($this->position <= 0) {
            $this->position = self::getTopPosition() + 1;
        }

        return parent::add($auto_date, $null_values);
    }

    public function update($null_values = false)
    {
        $seoUrls = [];
        foreach ($this->seo_url as $key => $item) {
            $seoUrls[$key] = Tools::link_rewrite($item);
        }
        if (!$this->checkSeoUrlUnique($seoUrls, $this->id)) {
            throw new PrestaShopException('Friendly url field must be unique');
        }

        return parent::update($null_values);
    }

    public static function getTopPosition()
    {
        $dbquery = new DbQuery();
        $dbquery->select('MAX(`position`)');
        $dbquery->from('blog_category', 'bc');
        $position = DB::getInstance()->getValue($dbquery);

        return (is_numeric($position)) ? $position : -1;
    }

    public static function getCategoryBySeoUrl($shopIds, $langId, $categoryUrl)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bcs.id_shop IN (' . $shopIds . ')');
        $dbquery->where("bcl.seo_url = '$categoryUrl'");

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);
    }

    public static function getCategoryData($shopIds, $langId, $categoryUrl)
    {
        $dbquery = new DbQuery();
        $dbquery->select(
            'bcl.`title`, bcl.`description`, bcl.`seo_url`, bcl.`meta_title`, bcl.`meta_description`,'
            .' bc.`id_blog_category`'
        );
        $dbquery->from('blog_category', 'bc');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bcs.id_shop IN (' . $shopIds . ')');
        $dbquery->where("bcl.seo_url = '$categoryUrl'");

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($dbquery);
    }

    public static function getAllCategories($shopIds, $langId, $categoryId = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`, bcl.`title`, bc.`id_parent_category`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bcs.id_shop IN (' . $shopIds . ')');
        if ($categoryId) {
            $dbquery->where('bc.id_blog_category != ' . $categoryId);
        }
        $dbquery->groupBy('bc.`id_blog_category`');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);
    }

    public static function getRootCategories($shopIds, $langId, $selectedId = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`, bcl.`title`, bc.`id_parent_category`, bcl.`seo_url`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bcs.id_shop IN (' . $shopIds . ')');
        if ($selectedId) {
            $dbquery->where('bc.id_blog_category = ' . $selectedId);
        } else {
            $dbquery->where('bc.id_parent_category = 0');
        }
        $dbquery->groupBy('bc.`id_blog_category`');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);
    }

    public static function countCategoryHasPosts($categoryId)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bp.`id_blog_post`');
        $dbquery->from('blog_post', 'bp');
        $dbquery->where('bp.id_blog_category = ' . $categoryId);

        return count(Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery));
    }

    public static function countCategoryActivePosts($categoryId)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bp.`id_blog_post`');
        $dbquery->from('blog_post', 'bp');
        $dbquery->where('bp.id_blog_category = ' . $categoryId);
        $dbquery->where('bp.active = 1');

        return count(Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery));
    }

    public static function getChildCategories($categoryId, $langId)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`, bcl.`title`, bc.`id_parent_category`, bcl.`seo_url`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bc.id_parent_category = ' . $categoryId);
        $dbquery->groupBy('bc.`id_blog_category`');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);
    }

    public static function getLastRootCategory()
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->where('bc.id_parent_category = 0');

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery);

        return count($result) === 1 ? reset($result) : false;
    }

    public static function countBlogCategory()
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`');
        $dbquery->from('blog_category', 'bc');

        return count(Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery));
    }

    public function updatePosition($way, $position)
    {
        if (!$res = $this->getPostPositions()) {
            return false;
        }
        if (!empty($res)) {
            foreach ($res as $item) {
                if ((int)$item['id_blog_category'] === (int)$this->id) {
                    $movedBlogPost = $item;
                }
            }
        }
        if (!isset($movedBlogPost) || !isset($position)) {
            return false;
        }
        $queryx = ' UPDATE `'._DB_PREFIX_.'blog_category`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
                ? '> '.(int)$movedBlogPost['position'].' AND `position` <= '.(int)$position
                : '< '.(int)$movedBlogPost['position'].' AND `position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'blog_category`
        SET `position` = '.(int)$position.'
        WHERE `id_blog_category` = '.(int)$movedBlogPost['id_blog_category'];

        return (Db::getInstance()->execute($queryx) && Db::getInstance()->execute($queryy));
    }

    protected function getPostPositions()
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`, bc.`position`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->orderBy('bc.`position`');

        return DB::getInstance()->executeS($dbquery);
    }


    protected function checkSeoUrlUnique($seoUrls, $id = false)
    {
        foreach ($seoUrls as $item) {
            if (!$this->sameSeoUrl($item, $id)) {
                return false;
            }
        }

        return true;
    }

    protected function sameSeoUrl($url, $categoryId = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bc.`id_blog_category`');
        $dbquery->from('blog_category', 'bc');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where("bcl.seo_url LIKE '$url'");
        if ($categoryId) {
            $dbquery->where('bc.id_blog_category != ' . $categoryId);
        }

        return count(Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery)) === 0;
    }
}
