<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

class BlogPost extends ObjectModel
{
    public $id;

    public $id_blog_category;

    public $active;

    public $post_position;

    public $post_author;

    public $post_title;

    public $post_description;

    public $post_content;

    public $date_add;

    public $post_meta_title;

    public $post_meta_description;

    public $post_seo_url;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'blog_post',
        'primary' => 'id_blog_post',
        'multilang' => true,
        'fields' => array(
            'id_blog_category' =>       array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'active' =>                 array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'post_author' =>            array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 64),
            'date_add' =>               array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'post_position' =>          array(
                'type' => self::TYPE_INT,
                'validate' => 'isunsignedInt',
                'required' => true
            ),
            // Lang fields
            'post_title' =>             array(
                'type' => self::TYPE_STRING,
                'required' => true,
                'validate' => 'isString',
                'lang' => true,
                'size' => 64
            ),
            'post_description' =>       array(
                'type' => self::TYPE_STRING,
                'required' => true,
                'validate' => 'isString',
                'lang' => true,
                'size' => 256
            ),
            'post_content' =>           array(
                'type' => self::TYPE_HTML,
                'required' => true,
                'validate' => 'isCleanHtml',
                'lang' => true
            ),
            'post_meta_title' =>         array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'size' => 64
            ),
            'post_meta_description' =>   array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'size' => 128
            ),
            'post_seo_url' =>           array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isString',
                'required' => true,
                'size' => 64
            ),
        ),
    );

    public function __construct($id = null, $lang = null, $shop = null)
    {
        parent::__construct($id, $lang, $shop);
    }

    public function add($autodate = true, $null_values = false)
    {
        if (!$this->checkSeoUrlUnique($this->post_seo_url)) {
            throw new PrestaShopException('Friendly url field must be unique');
        }
        if ($this->post_position <= 0) {
            $this->post_position = self::getTopPosition() + 1;
        }

        return parent::add($autodate, $null_values) && Validate::isLoadedObject($this);
    }

    public function update($null_values = false)
    {
        $seoUrls = [];
        foreach ($this->post_seo_url as $key => $item) {
            $seoUrls[$key] = Tools::link_rewrite($item);
        }
        if (!$this->checkSeoUrlUnique($seoUrls, $this->id)) {
            throw new PrestaShopException('Friendly url field must be unique');
        }

        return parent::update($null_values);
    }

    public static function getPostsByUrl($shopId, $langId, $url)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bp.`id_blog_post`');
        $dbquery->from('blog_post', 'bp');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_post_lang` bpl ON bpl.`id_blog_post` = bp.`id_blog_post`');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bp.`id_blog_category`'
        );
        $dbquery->where('bcs.id_shop = ' . $shopId);
        $dbquery->where('bpl.id_lang = ' . $langId);
        $dbquery->where("bpl.post_seo_url = '$url'");

        return DB::getInstance()->executeS($dbquery);
    }

    public static function getPostsBySeoUrl($shopId, $langId, $url)
    {
        $dbquery = new DbQuery();
        $dbquery->select(
            'bcl.`title`, bc.`id_blog_category`, bp.`post_position`, bp.`post_author`, bpl.`post_title`'
            .', bpl.`post_description`, bpl.`post_content`, bpi.`image_name_with_ext`, bp.`id_blog_post`,'
            .' bp.`date_add`, bpl.`post_meta_title`, bpl.`post_meta_description`, bpl.`post_seo_url`, bcl.`seo_url`'
        );
        $dbquery->from('blog_post', 'bp');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_post_lang` bpl ON bpl.`id_blog_post` = bp.`id_blog_post`');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_post_image` bpi ON bpi.`id_blog_post` = bp.`id_blog_post`');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_category` bc ON bc.`id_blog_category` = bp.`id_blog_category`');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bcs.id_shop = ' . $shopId);
        $dbquery->where('bpl.id_lang = ' . $langId);
        $dbquery->where("bpl.post_seo_url = '$url'");

        return DB::getInstance()->getRow($dbquery);
    }

    public static function getPostsForBlogByShopId($shopId, $langId, $n, $p, $categoryId = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select(
            'bcl.`title`, bc.`id_blog_category`, bp.`post_position`, bp.`post_author`, bpl.`post_title`'
            .', bpl.`post_description`, bpl.`post_content`, bpi.`image_card_name_with_ext`, bp.`id_blog_post`,'
            .' bpl.`post_seo_url`, bcl.`seo_url`, bp.`date_add`'
        );
        $dbquery->from('blog_post', 'bp');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_post_lang` bpl ON bpl.`id_blog_post` = bp.`id_blog_post`');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_post_image` bpi ON bpi.`id_blog_post` = bp.`id_blog_post`');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_category` bc ON bc.`id_blog_category` = bp.`id_blog_category`');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_lang` bcl ON bcl.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcl.id_lang = ' . $langId);
        $dbquery->where('bcs.id_shop = ' . $shopId);
        $dbquery->where('bpl.id_lang = ' . $langId);
        $dbquery->where('bp.active = 1');
        if (!empty($categoryId)) {
            $dbquery->where('bc.id_blog_category = ' . $categoryId);
        }

        $dbquery->orderBy('bp.`post_position`');
        $dbquery->limit((int)$n, (((int)$p - 1) * (int)$n));

        return DB::getInstance()->executeS($dbquery);
    }

    public static function getPostsCountForBlog($shopId, $categoryId = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bp.`id_blog_post`');
        $dbquery->from('blog_post', 'bp');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_category` bc ON bc.`id_blog_category` = bp.`id_blog_category`');
        $dbquery->join(
            'LEFT JOIN `' . _DB_PREFIX_ . 'blog_category_shop` bcs ON bcs.`id_blog_category` = bc.`id_blog_category`'
        );
        $dbquery->where('bcs.id_shop = ' . $shopId);
        $dbquery->where('bp.active = 1');
        if (!empty($categoryId)) {
            $dbquery->where('bc.id_blog_category = ' . $categoryId);
        }

        return DB::getInstance()->executeS($dbquery);
    }

    public static function getTopPosition()
    {
        $dbquery = new DbQuery();
        $dbquery->select('MAX(`post_position`)');
        $dbquery->from('blog_post', 'bp');
        $position = DB::getInstance()->getValue($dbquery);

        return (is_numeric($position)) ? $position : -1;
    }

    public function updatePosition($way, $position)
    {
        if (!$res = $this->getPostPositions()) {
            return false;
        }
        if (!empty($res)) {
            foreach ($res as $item) {
                if ((int)$item['id_blog_post'] === (int)$this->id) {
                    $movedBlogPost = $item;
                }
            }
        }
        if (!isset($movedBlogPost) || !isset($position)) {
            return false;
        }
        $queryx = ' UPDATE `'._DB_PREFIX_.'blog_post`
        SET `post_position`= `post_position` '.($way ? '- 1' : '+ 1').'
        WHERE `post_position`
        '.($way
                ? '> '.(int)$movedBlogPost['post_position'].' AND `post_position` <= '.(int)$position
                : '< '.(int)$movedBlogPost['post_position'].' AND `post_position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'blog_post`
        SET `post_position` = '.(int)$position.'
        WHERE `id_blog_post` = '.(int)$movedBlogPost['id_blog_post'];

        return (Db::getInstance()->execute($queryx) && Db::getInstance()->execute($queryy));
    }

    protected function getPostPositions()
    {
        $dbquery = new DbQuery();
        $dbquery->select('bp.`id_blog_post`, bp.`post_position`');
        $dbquery->from('blog_post', 'bp');
        $dbquery->orderBy('bp.`post_position`');

        return DB::getInstance()->executeS($dbquery);
    }

    protected function checkSeoUrlUnique($seoUrls, $id = false)
    {
        foreach ($seoUrls as $item) {
            if (!$this->sameSeoUrl($item, $id)) {
                return false;
            }
        }

        return true;
    }

    protected function sameSeoUrl($url, $postId = false)
    {
        $dbquery = new DbQuery();
        $dbquery->select('bp.`id_blog_post`');
        $dbquery->from('blog_post', 'bp');
        $dbquery->join('LEFT JOIN `' . _DB_PREFIX_ . 'blog_post_lang` bpl ON bpl.`id_blog_post` = bp.`id_blog_post`');
        $dbquery->where("bpl.post_seo_url LIKE '$url'");
        if ($postId) {
            $dbquery->where('bp.id_blog_post != ' . $postId);
        }

        return count(Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery)) === 0;
    }
}
