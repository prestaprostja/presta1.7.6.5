<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'prestaprosblog/classes/BlogCategory.php');
include_once(_PS_MODULE_DIR_.'prestaprosblog/classes/BlogPost.php');
include_once(_PS_MODULE_DIR_.'prestaprosblog/classes/BlogPostImage.php');

class PrestaProsBlog extends Module
{
    const CONFIG_RULE = 'prestaprosblogrule';
    const CONFIG_TITLE = 'prestaprosblogtitle';
    const CONFIG_DESCRIPTION = 'prestaprosblogdescription';
    const CONFIG_META_TITLE = 'prestaprosblogmetatitle';
    const CONFIG_META_DESCRIPTION = 'prestaprosblogmetadescription';
    const CONFIG_POST_PER_PAGE = 'prestaprosblogpostperpage';
    const SUBMIT_SETTINGS = 'submitSettings';
    const GENERAL_TAB = 'Adminprestaprosblogdashboard';
    const TABS = array(
        array(
            'class_name' => 'AdminPrestaProsBlogPost',
            'name' => 'Blog Posts',
        ),
        array(
            'class_name' => 'AdminPrestaProsBlogCategory',
            'name' => 'Blog Categories',
        ),
        array(
            'class_name' => 'AdminPrestaProsBlogSettings',
            'name' => 'Blog Settings',
        ),
    );

    private $languages;

    public function __construct()
    {
        $this->name = 'prestaprosblog';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'PrestaPros.com';
        $this->need_instance = 0;
        $this->languages = Language::getLanguages();
        $this->controllers = array(
            'AdminPrestaProsBlogCategoryController',
            'AdminPrestaProsBlogPostController',
            'AdminPrestaProsBlogSettingsController',
            'blog',
            'blogcategory',
            'blogpost',
        );
        $this->ps_versions_compliancy = array(
            'min' => '1.7',
            'max' => _PS_VERSION_
        );
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('PrestaPros blog module');
        $this->description = $this->l('PrestaProsBlog Powerfull Prestashop Blog Module by PrestaPros');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        if (!parent::install()
            || !$this->registerTabs()
            || !$this->registerHook('moduleRoutes')
        ) {
            return false;
        }
        $this->setDefaultSettings();
        $this->addDefaultBlogCategory();

        return true;
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');
        if (!parent::uninstall()
            || !$this->unRegisterTabs()
        ) {
            return false;
        }

        return true;
    }

    public function hookModuleRoutes()
    {
        $blogRule = Configuration::get(self::CONFIG_RULE, null, null, $this->context->shop->id);
        $blogRule = !empty($blogRule) ? $blogRule :'blog';

        return array(
            'module-prestaprosblog-blog' => array(
                'controller' => 'blog',
                'rule' => $blogRule,
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'prestaprosblog',
                    'controller' => 'blog'
                )
            ),
            'module-prestaprosblog-blog-category' => array(
                'controller' => 'blogcategory',
                'rule' => $blogRule . '/category{/:page}',
                'keywords' => array(
                    'page' => array(
                        'regexp' => '[_a-zA-Z0-9-\pL]*',
                        'param' => 'page'
                    ),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'prestaprosblog',
                    'controller' => 'blogcategory'
                )
            ),
            'module-prestaprosblog-blog-post' => array(
                'controller' => 'blogpost',
                'rule' => $blogRule . '/post{/:page}',
                'keywords' => array(
                    'page' => array(
                        'regexp' => '[_a-zA-Z0-9-\pL]*',
                        'param' => 'page'
                    ),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'prestaprosblog',
                    'controller' => 'blogpost'
                )
            ),
        );
    }

    public function getContent()
    {
        if (((bool)Tools::isSubmit(self::SUBMIT_SETTINGS)) == true) {
            $this->updateSettingField(self::CONFIG_RULE);
            $this->updateSettingField(self::CONFIG_POST_PER_PAGE, true);
            $this->updateSettingField(self::CONFIG_TITLE);
            $this->updateSettingFieldNotRequired(self::CONFIG_DESCRIPTION);
            $this->updateSettingField(self::CONFIG_META_TITLE);
            $this->updateSettingFieldNotRequired(self::CONFIG_META_DESCRIPTION);
        }

        return $this->renderForm();
    }

    public function getPostImage($postId)
    {
        return BlogPostImage::getImageByPostId($postId);
    }

    public function getPosts($n, $p)
    {
        $posts = BlogPost::getPostsForBlogByShopId($this->context->shop->id, $this->context->language->id, $n, $p);

        return count($posts) === 0 ? false : $this->preparePostsData($posts);
    }

    public function getCategories($selectedId = false)
    {
        $rootCategories = BlogCategory::getRootCategories(
            $this->context->shop->id,
            $this->context->language->id,
            $selectedId
        );
        $categoriesData = array();
        $this->prepareCategoryTree($categoriesData, $rootCategories, $selectedId, 3);

        return implode('', $categoriesData);
    }

    public function isTheLastRootCategory($categoryId)
    {
        if ($categoryId) {
            $lastRootCategory = BlogCategory::getLastRootCategory();
            return (int)$lastRootCategory['id_blog_category'] === (int)$categoryId;
        }

        return false;
    }

    public function countActivePost()
    {
        $posts = BlogPost::getPostsCountForBlog($this->context->shop->id);

        return count($posts);
    }

    public function countActivePostByCategoryId($categoryId)
    {
        $posts = BlogPost::getPostsCountForBlog($this->context->shop->id, $categoryId);

        return count($posts);
    }

    public function getPostsByCategory($categoryId, $n, $p)
    {
        $posts = BlogPost::getPostsForBlogByShopId(
            $this->context->shop->id,
            $this->context->language->id,
            $n,
            $p,
            $categoryId
        );

        return count($posts) === 0 ? false : $this->preparePostsData($posts);
    }

    public function getPostsByUrl($postUrl)
    {
        $post = BlogPost::getPostsBySeoUrl($this->context->shop->id, $this->context->language->id, $postUrl);

        return empty($post) ? false : $this->preparePostData($post);
    }

    public function checkCategoryExist($categoryUrl)
    {
        $result = BlogCategory::getCategoryBySeoUrl($this->context->shop->id, $this->context->language->id, $categoryUrl);

        return count($result) === 1;
    }

    public function checkPostExist($postUrl)
    {
        $result = BlogPost::getPostsByUrl($this->context->shop->id, $this->context->language->id, $postUrl);

        return count($result) === 1;
    }

    public function prepareBlogCategoryData($categoryUrl)
    {
        return BlogCategory::getCategoryData($this->context->shop->id, $this->context->language->id, $categoryUrl);
    }

    public function getBlogUrl($itemId = false, $prefix = false)
    {
        $blogRule = Configuration::get(self::CONFIG_RULE, null, null, $this->context->shop->id);
        $blogRule = !empty($blogRule) ? $blogRule : 'blog';
        $baseUrl = $this->context->shop->getBaseURL(true) . $blogRule;
        if ($prefix) {
            $baseUrl .= $prefix;
        }
        if ($itemId) {
            $baseUrl .= $itemId;
        }

        return $baseUrl;
    }

    protected function prepareCategoryTree(&$categoriesData, $categories, $selectedId, $limit)
    {
        $categoriesData[] = '<ul>';
        foreach ($categories as $item) {
            $postCount = BlogCategory::countCategoryActivePosts($item['id_blog_category']);
            $childCategories = BlogCategory::getChildCategories($item['id_blog_category'], $this->context->language->id);
            if ($postCount === 0 && count($childCategories) === 0) {
                continue;
            }
            if ($limit === 0) {
                $categoriesData[] = '</ul>';
                return;
            }
            $limit--;
            $categoriesData[] = '<li>';
            $categoryName = $postCount === 0 ? $item['title'] : $item['title'] . " ($postCount)";
            $categoryUrl = $this->getBlogUrl($item['seo_url'], '/category/');
            if (is_int($selectedId) && $selectedId === (int)$item['id_blog_category']) {
                $categoriesData[] = "<a style='color: blue; text-decoration: underline' href='$categoryUrl'>$categoryName</a>";
            } else {
                $categoriesData[] = $postCount === 0 ? "<span style='color: grey;'>$categoryName</span>" : "<a style='color: black' href='$categoryUrl'>$categoryName</a>";
            }
            if (count($childCategories) > 0) {
                $this->prepareCategoryTree($categoriesData, $childCategories, $selectedId, $limit);
            }
            $categoriesData[] = '</li>';
        }
        $categoriesData[] = '</ul>';
    }

    protected function preparePostsData($posts)
    {
        foreach ($posts as $index => $item) {
            $posts[$index] = $this->preparePostData($item);
        }

        return $posts;
    }

    protected function preparePostData($item)
    {
        $item['date_add'] = date("d-m-Y", strtotime($item['date_add']));
        $item['image_url'] = false;
        if (isset($item['image_name_with_ext']) && !empty($item['image_name_with_ext'])) {
            $item['image_url'] = _MODULE_DIR_
                . $this->name
                . '/views/img/images/'
                . $item['image_name_with_ext'];
        }
        if (isset($item['image_card_name_with_ext']) && !empty($item['image_card_name_with_ext'])) {
            $item['image_url'] = _MODULE_DIR_
                . $this->name
                . '/views/img/images/'
                . $item['image_card_name_with_ext'];
        }
        $item['category_url'] = $this->getBlogUrl($item['seo_url'], '/category/'
        );
        $item['post_url'] = $this->getBlogUrl($item['post_seo_url'], '/post/');

        return $item;
    }

    protected function updateSettingField($key, $intOnly = false)
    {
        if ($intOnly) {
            if (!empty(Tools::getValue($key)) && (int)Tools::getValue($key) > 0) {
                Configuration::updateValue(
                    $key,
                    (int)Tools::getValue($key),
                    false,
                    null,
                    $this->context->shop->id
                );
            }
        } else {
            if (!empty(Tools::getValue($key))) {
                Configuration::updateValue(
                    $key,
                    Tools::getValue($key),
                    false,
                    null,
                    $this->context->shop->id
                );
            }
        }
    }

    protected function updateSettingFieldNotRequired($key)
    {
        $val = !empty(Tools::getValue($key)) ? Tools::getValue($key) : '';
        Configuration::updateValue($key, $val, false, null, $this->context->shop->id);
    }

    protected function setDefaultSettings()
    {
        Configuration::updateValue(self::CONFIG_RULE, 'blog');
        Configuration::updateValue(self::CONFIG_POST_PER_PAGE, 6);
        Configuration::updateValue(self::CONFIG_TITLE, 'Blog');
        Configuration::updateValue(self::CONFIG_DESCRIPTION, '');
        Configuration::updateValue(self::CONFIG_META_TITLE, 'Blog');
        Configuration::updateValue(self::CONFIG_META_DESCRIPTION, '');
    }

    protected function registerTabs()
    {
        $parentTabId = $this->registerParentTab();
        foreach (self::TABS as $tabData) {
            $tab = new Tab();
            $tab->class_name = $tabData['class_name'];
            $tab->id_parent = $parentTabId;
            $tab->module = $this->name;
            foreach($this->languages as $l) {
                $tab->name[$l['id_lang']] = $this->l($tabData['name']);
            }
            $tab->save();
        }

        return true;
    }

    protected function registerParentTab()
    {
        $tab = new Tab();
        $parentId = (int)Tab::getIdFromClassName("IMPROVE");
        $tab->class_name = self::GENERAL_TAB;
        $tab->id_parent = $parentId;
        $tab->module = $this->name;
        foreach($this->languages as $l) {
            $tab->name[$l['id_lang']] = $this->l("PrestaPros Blog");
        }

        return $tab->save() ? $tab->id : $parentId;
    }

    protected function unRegisterTabs()
    {
        foreach (self::TABS as $tabData) {
            $tabId = Tab::getIdFromClassName($tabData['class_name']);
            if(isset($tabId) && !empty($tabId)) {
                $tab = new Tab($tabId);
                $tab->delete();
            }
        }
        $this->unRegisterParentTab();

        return true;
    }

    protected function unRegisterParentTab()
    {
        $tabId = Tab::getIdFromClassName(self::GENERAL_TAB);
        $tab = new Tab($tabId);
        $tab->delete();
    }

    protected function renderForm()
    {
        $helper = new HelperForm();
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = self::SUBMIT_SETTINGS;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(
                self::CONFIG_RULE => Configuration::get(
                    self::CONFIG_RULE,
                    null,
                    null,
                    $this->context->shop->id
                ),
                self::CONFIG_POST_PER_PAGE => Configuration::get(
                    self::CONFIG_POST_PER_PAGE,
                    null,
                    null,
                    $this->context->shop->id
                ),
                self::CONFIG_TITLE => Configuration::get(
                    self::CONFIG_TITLE,
                    null,
                    null,
                    $this->context->shop->id
                ),
                self::CONFIG_DESCRIPTION => Configuration::get(
                    self::CONFIG_DESCRIPTION,
                    null,
                    null,
                    $this->context->shop->id
                ),
                self::CONFIG_META_TITLE => Configuration::get(
                    self::CONFIG_META_TITLE,
                    null,
                    null,
                    $this->context->shop->id
                ),
                self::CONFIG_META_DESCRIPTION => Configuration::get(
                    self::CONFIG_META_DESCRIPTION,
                    null,
                    null,
                    $this->context->shop->id
                ),
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => Configuration::get('PS_LANG_DEFAULT'),
        );

        return $helper->generateForm(array($this->getForm()));
    }

    protected function getForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Blog Settings')
                ),
                'input' => array(
                    array(
                        'label' => $this->l('Post per page (default: 6)'),
                        'type' => 'text',
                        'name' => self::CONFIG_POST_PER_PAGE,
                    ),
                    array(
                        'label' => $this->l('Basic blog address (default: blog)'),
                        'type' => 'text',
                        'name' => self::CONFIG_RULE,
                    ),
                    array(
                        'label' => $this->l('Blog Title (default: Blog)'),
                        'type' => 'text',
                        'name' => self::CONFIG_TITLE,
                    ),
                    array(
                        'label' => $this->l('Blog Description'),
                        'type' => 'text',
                        'name' => self::CONFIG_DESCRIPTION,
                    ),
                    array(
                        'label' => $this->l('Blog Meta Title (default: Blog)'),
                        'type' => 'text',
                        'name' => self::CONFIG_META_TITLE,
                    ),
                    array(
                        'label' => $this->l('Blog Meta Description'),
                        'type' => 'text',
                        'name' => self::CONFIG_META_DESCRIPTION,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function addDefaultBlogCategory()
    {
        $blogCategory = new BlogCategory();
        foreach ($this->languages as $language) {
            $blogCategory->title[$language['id_lang']] = $this->l('Sample category');
            $blogCategory->seo_url[$language['id_lang']] = Tools::link_rewrite($this->l('sample category'));
            $blogCategory->position = 1;
            $blogCategory->id_parent_category = 0;
        }
        $blogCategory->add();
    }
}
