{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from PrestaProsBlog
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@prestapros.com
*
* @author    PrestaPros.com
* @copyright Copyright (c) 2017-2020 PrestaPros
* @license   Commercial license
* @package   prestaprosblog
*}
{extends file='customer/page.tpl'}
{block name='head_seo'}
    <title>{block name='head_seo_title'}{$post.post_meta_title}{/block}</title>
    <meta name="description" content="{block name='head_seo_description'}{$post.post_meta_description}{/block}">
{/block}
{block name='page_content'}
    {if isset($error)}
    <header class="page-header">
        <h1 class="h1 page-title">
            {$error}
        </h1>
    </header>
    {else}
    <div class="col-md-3">
        {include file='module:prestaprosblog/views/templates/front/_partials/categories.tpl'}
    </div>
    <div class="col-md-9">
        <img src="{$post.image_url}" alt="{$post.image_name_with_ext}">
        <h1 style="margin-top: 10px;">{$post.post_title}</h1>
        <div style="word-wrap: break-word;">{$post.post_content nofilter}</div>
        <div>
            <span class="badge badge-success">{l s='Posted' d='prestaprosblog'}: {$post.date_add}</span>
            {if $post.post_author}
                <div class="pull-right">
                    <span class="label">{l s='Author' d='prestaprosblog'}: {$post.post_author}</span>
                </div>
            {/if}
        </div>
        <hr>
    </div>
    {/if}
{/block}
