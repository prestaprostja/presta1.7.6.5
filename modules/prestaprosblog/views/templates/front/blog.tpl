{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from PrestaProsBlog
* Use, copy, modification or distribution of this source file without written
* license agreement from the MigrationPro is strictly forbidden.
* In order to obtain a license, please contact us: contact@prestapros.com
*
* @author    PrestaPros.com
* @copyright Copyright (c) 2017-2020 PrestaPros
* @license   Commercial license
* @package   prestaprosblog
*}
{extends file='customer/page.tpl'}
{block name='head_seo'}
    <title>{block name='head_seo_title'}{$blog_meta_title}{/block}</title>
    <meta name="description" content="{block name='head_seo_description'}{$blog_meta_description}{/block}">
{/block}
{block name='page_content'}
    {if isset($error)}
        <header class="page-header">
            <h1 class="h1 page-title">
                {$error}
            </h1>
        </header>
    {else}
        <header class="page-header">
            <h1 class="h1 page-title">
                {$blog_title}
            </h1>
            <p>{$blog_description}</p>
        </header>
        <div class="col-md-3">
            {include file='module:prestaprosblog/views/templates/front/_partials/categories.tpl'}
        </div>
        <div class="col-md-9">
            {if isset($posts)}
                {foreach from=$posts item=post}
                    {include file='module:prestaprosblog/views/templates/front/_partials/post_list.tpl'}
                {/foreach}
            {else}
                {foreach from=$category_posts item=post}
                    {include file='module:prestaprosblog/views/templates/front/_partials/post_list.tpl'}
                {/foreach}
            {/if}
            {include file='module:prestaprosblog/views/templates/front/_partials/page_navigation.tpl'}
        </div>
    {/if}
{/block}
