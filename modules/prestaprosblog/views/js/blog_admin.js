/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

$( document ).ready(function() {
    $('input').on('input',function(e) {
        let arrayInputId = $(this).attr('id').split("_");
        if (arrayInputId[0] === 'title') {
            let urlId = '#seo_url_' + arrayInputId[1];
            if (!$(urlId).val() || $(urlId).val() === $(this).val().slice(0,-1)) {
                $(urlId).val($(this).val());
            }
        }
        if (arrayInputId[0] === 'post' && arrayInputId[1] === 'title') {
            let urlPostId = '#post_seo_url_' + arrayInputId[2];
            if (!$(urlPostId).val() || $(urlPostId).val() === $(this).val().slice(0,-1)) {
                $(urlPostId).val($(this).val());
            }
        }
    });
});
