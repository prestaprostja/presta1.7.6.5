<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from DataFeedWatch
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the DataFeedWatch is strictly forbidden.
 * In order to obtain a license, please contact us: DataFeedWatch.com
 *
 * @author    DataFeedWatch
 * @copyright Copyright (c) 2017-2020 DataFeedWatch
 * @license   Commercial license
 * @package   DataFeedWatchResponseModule
 */

class PrestaProsBlogBlogPostModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        if (!empty(Tools::getValue('page')) && $this->module->checkPostExist(Tools::getValue('page'))) {
            $postData = $this->module->getPostsByUrl(Tools::getValue('page'));
            $this->context->smarty->assign(array(
                'post' => $postData,
                'categories' => $this->module->getCategories((int)$postData['id_blog_category']),
                'blog_head_url' => $this->module->getBlogUrl(),
                'head_page' => false,
            ));
        } else {
            $this->context->smarty->assign(array(
                'error' => $this->l('Post not exist'),
            ));
        }

        $this->setTemplate('module:' . $this->module->name . '/views/templates/front/blog_post.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/blog.css');
    }
}
