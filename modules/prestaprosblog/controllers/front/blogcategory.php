<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from DataFeedWatch
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the DataFeedWatch is strictly forbidden.
 * In order to obtain a license, please contact us: DataFeedWatch.com
 *
 * @author    DataFeedWatch
 * @copyright Copyright (c) 2017-2020 DataFeedWatch
 * @license   Commercial license
 * @package   DataFeedWatchResponseModule
 */

class PrestaProsBlogBlogCategoryModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        if (!empty(Tools::getValue('page')) && $this->module->checkCategoryExist(Tools::getValue('page'))) {
            $postPerPage = (int)Configuration::get(PrestaProsBlog::CONFIG_POST_PER_PAGE);
            $n = (isset($postPerPage) && !empty($postPerPage)) ? $postPerPage : 12;
            $p = !empty(Tools::getValue('p')) ? Tools::getValue('p') : 1;
            $categoryData = $this->module->prepareBlogCategoryData(Tools::getValue('page'));
            $this->initPagination($n, $p, $categoryData['id_blog_category'], $categoryData['seo_url']);

            $this->context->smarty->assign(array(
                'category_posts' => $this->module->getPostsByCategory($categoryData['id_blog_category'], $n, $p),
                'blog_title' => $categoryData['title'],
                'blog_description' => $categoryData['description'],
                'blog_meta_title' => $categoryData['meta_title'],
                'blog_meta_description' => $categoryData['meta_description'],
                'categories' => $this->module->getCategories((int)$categoryData['id_blog_category']),
                'blog_head_url' => $this->module->getBlogUrl(),
                'head_page' => false,
            ));
        } else {
            $this->context->smarty->assign(array(
                'error' => $this->l('Category not exist'),
            ));
        }
        $this->setTemplate('module:' . $this->module->name . '/views/templates/front/blog.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/blog.css');
    }

    protected function initPagination($n, $p, $categoryId, $categorySeoUrl)
    {
        $pagesNb = ceil($this->module->countActivePostByCategoryId($categoryId) / (int)$n);
        $range = 2;
        $start = (int)($p - $range);
        if ($start < 1) {
            $start = 1;
        }
        $stop = (int)($p + $range);
        if ($stop > $pagesNb) {
            $stop = (int)$pagesNb;
        }

        $this->context->smarty->assign(array(
            'pagination' => array(
                'pages_nb' => $pagesNb,
                'prev_p' => $p != 1 ? $p - 1 : 1,
                'next_p' => (int)$p + 1  > $pagesNb ? $pagesNb : $p + 1,
                'requestPage' => $this->module->getBlogUrl($categorySeoUrl, '/category/'),
                'p' => $p,
                'n' => $n,
                'range' => $range,
                'start' => $start,
                'stop' => $stop,
            )
        ));
    }
}
