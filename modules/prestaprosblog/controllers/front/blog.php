<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from DataFeedWatch
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the DataFeedWatch is strictly forbidden.
 * In order to obtain a license, please contact us: DataFeedWatch.com
 *
 * @author    DataFeedWatch
 * @copyright Copyright (c) 2017-2020 DataFeedWatch
 * @license   Commercial license
 * @package   DataFeedWatchResponseModule
 */

class PrestaProsBlogBlogModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $postPerPage = (int)Configuration::get(PrestaProsBlog::CONFIG_POST_PER_PAGE);
        $n = (isset($postPerPage) && !empty($postPerPage)) ? $postPerPage : 12;
        $p = !empty(Tools::getValue('p')) ? Tools::getValue('p') : 1;
        $this->initPagination($n, $p);

        $this->context->smarty->assign(array(
            'posts' =>  $this->module->getPosts($n, $p),
            'blog_title' => Configuration::get(
                PrestaProsBlog::CONFIG_TITLE,
                null,
                null,
                $this->context->shop->id
            ),
            'blog_description' => Configuration::get(
                PrestaProsBlog::CONFIG_DESCRIPTION,
                null,
                null,
                $this->context->shop->id
            ),
            'blog_meta_title' => Configuration::get(
                PrestaProsBlog::CONFIG_META_TITLE,
                null,
                null,
                $this->context->shop->id
            ),
            'blog_meta_description' => Configuration::get(
                PrestaProsBlog::CONFIG_META_DESCRIPTION,
                null,
                null,
                $this->context->shop->id
            ),
            'categories' => $this->module->getCategories(),
            'blog_head_url' => $this->module->getBlogUrl(),
            'head_page' => true,
        ));

        $this->setTemplate('module:' . $this->module->name . '/views/templates/front/blog.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/blog.css');
    }

    protected function initPagination($n, $p)
    {
        $pagesNb = ceil($this->module->countActivePost() / (int)$n);
        $range = 2;
        $start = (int)($p - $range);
        if ($start < 1) {
            $start = 1;
        }
        $stop = (int)($p + $range);
        if ($stop > $pagesNb) {
            $stop = (int)$pagesNb;
        }

        $this->context->smarty->assign(array(
            'pagination' => array(
                'pages_nb' => $pagesNb,
                'prev_p' => $p != 1 ? $p - 1 : 1,
                'next_p' => (int)$p + 1  > $pagesNb ? $pagesNb : $p + 1,
                'requestPage' => $this->module->getBlogUrl(),
                'p' => $p,
                'n' => $n,
                'range' => $range,
                'start' => $start,
                'stop' => $stop,
            )
        ));
    }
}
