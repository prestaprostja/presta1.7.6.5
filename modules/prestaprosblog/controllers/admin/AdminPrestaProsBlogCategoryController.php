<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminPrestaProsBlogCategoryController extends ModuleAdminController
{
    protected $position_identifier = 'id_blog_category';

    public function __construct()
    {
        $this->table = 'blog_category';
        $this->className = 'BlogCategory';
        $this->lang = true;
        $this->deleted = false;
        $this->module = 'prestaprosblog';
        $this->explicitSelect = true;
        $this->_defaultOrderBy = 'position';
        $this->allow_export = false;
        $this->_defaultOrderWay = 'ASC';
        $this->bootstrap = true;
        if (Shop::isFeatureActive()) {
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
        }
        parent::__construct();

        $this->fields_list = array(
            'id_blog_category' => array(
                'title' => $this->l('ID'),
                'type' => 'text',
            ),
            'position' => array(
                'title' => $this->l('Position'),
                'align' => 'left',
                'position' => 'position',
            ),
            'title' => array(
                'title' => $this->l('Category Title'),
                'type' => 'text',
            ),
            'id_parent_category' => array(
                'title' => $this->l('Parent category id'),
                'width' => 60,
                'type' => 'text',
            ),
        );
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        parent::__construct();
    }

    public function init()
    {
        parent::init();
        $this->_join = 'LEFT JOIN '
            . _DB_PREFIX_
            . 'blog_category_shop bcs ON a.id_blog_category=bcs.id_blog_category && bcs.id_shop IN('
            . implode(',', Shop::getContextListShopID()) . ')';
        $this->_select = 'bcs.id_shop';
        $this->_select = 'a.position position';
        $this->_defaultOrderBy = 'position';
        $this->_defaultOrderWay = 'ASC';
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP) {
            $this->_group = 'GROUP BY a.id_blog_category';
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJqueryPlugin('select2');
        $this->addJs(_MODULE_DIR_.$this->module->name.'/views/js/blog_admin.js');
    }

    public function renderList()
    {
        if (isset($this->_filter) && trim($this->_filter) == '') {
            $this->_filter = $this->original_filter;
        }
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }

    public function renderForm()
    {
        $parentCategory = array();
        $parentCategory[] = array("id_blog_category" => 0, 'title' => $this->l('Choose'));
        $categories = BlogCategory::getAllCategories(
            implode(',', Shop::getContextListShopID()),
            (int)Context::getContext()->cookie->id_lang,
            Tools::getValue('id_blog_category')
        );

        if (count($categories) > 0 && !$this->module->isTheLastRootCategory(Tools::getValue('id_blog_category'))) {
            $parentCategory = array_merge($parentCategory, $categories);
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Blog Category'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'lang' => true,
                    'required' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'description',
                    'id' => 'description',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta title'),
                    'name' => 'meta_title',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta description'),
                    'name' => 'meta_description',
                    'lang' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Parent category'),
                    'name' => 'id_parent_category',
                    'class' => 'chosen',
                    'col' => 5,
                    'options' => array(
                        'query' => $parentCategory,
                        'id' => 'id_blog_category',
                        'name' => 'title',
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Friendly url'),
                    'desc' => $this->l('Field must be unique to other category'),
                    'name' => 'seo_url',
                    'lang' => true,
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );
        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association:'),
                'name' => 'checkBoxShopAsso',
            );
        }

        $this->tpl_form_vars = array(
            'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
        );

        return parent::renderForm();
    }

    public function initToolbar()
    {
        parent::initToolbar();
    }

    public function processPosition()
    {
        if ($this->tabAccess['edit'] !== '1') {
            $this->errors[] = Tools::displayError('You do not have permission to edit this.');
        } elseif (!Validate::isLoadedObject(
            $object = new BlogCategory(
                (int)Tools::getValue($this->identifier, Tools::getValue('id_blog_category', 1))
            )
        )
        ) {
            $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.')
                .' <b>'. $this->table.'</b> '.Tools::displayError('(cannot load object)');
        }

        if (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position'))) {
            $this->errors[] = Tools::displayError('Failed to update the position.');
        } else {
            Tools::redirectAdmin(
                self::$currentIndex .'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&token='
                .Tools::getAdminTokenLite('AdminPrestaProsBlogCategoryController')
            );
        }
    }
    public function ajaxProcessUpdatePositions()
    {
        $id_blog_category = (int)(Tools::getValue('id'));
        $way = (int)(Tools::getValue('way'));
        $positions = Tools::getValue($this->table);
        if (is_array($positions)) {
            foreach ($positions as $key => $value) {
                $pos = explode('_', $value);
                if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_blog_category)) {
                    $position = $key + 1;
                    break;
                }
            }
        }
        $blogCategoryClass = new BlogCategory($id_blog_category);
        if (Validate::isLoadedObject($blogCategoryClass)) {
            if (isset($position) && $blogCategoryClass->updatePosition($way, $position)) {
                Hook::exec('action'.$this->className.'Update');
                die(true);
            } else {
                die('{"hasError" : true, errors : "Can not update blogCategory position"}');
            }
        } else {
            die('{"hasError" : true, "errors" : "This blogCategory can not be loaded"}');
        }
    }

    public function beforeAdd($object)
    {
        foreach ($this->context->controller->getLanguages() as $language) {
            $object->seo_url[$language['id_lang']] = Tools::link_rewrite($object->seo_url[$language['id_lang']]);
        }

        return parent::beforeAdd($object);
    }

    public function afterUpdate($object)
    {
        foreach ($this->context->controller->getLanguages() as $language) {
            $object->seo_url[$language['id_lang']] = Tools::link_rewrite($object->seo_url[$language['id_lang']]);
        }
        $object->update();

        return parent::afterUpdate($object);
    }
}
