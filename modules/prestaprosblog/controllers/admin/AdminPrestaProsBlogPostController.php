<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminPrestaProsBlogPostController extends ModuleAdminController
{
    protected $position_identifier = 'id_blog_post';

    public function __construct()
    {
        $this->table = 'blog_post';
        $this->className = 'BlogPost';
        $this->lang = true;
        $this->deleted = false;
        $this->module = 'prestaprosblog';
        $this->explicitSelect = true;
        $this->_defaultOrderBy = 'position';
        $this->allow_export = false;
        $this->_defaultOrderWay = 'ASC';
        $this->bootstrap = true;

        parent::__construct();

        $this->fields_list = array(
            'id_blog_post' => array(
                'title' => $this->l('ID'),
                'width' => 100,
                'type' => 'text',
            ),
            'position' => array(
                'title' => $this->l('Position'),
                'align' => 'left',
                'position' => 'position',
            ),
            'post_title' => array(
                'title' => $this->l('Post Title'),
                'width' => 60,
                'type' => 'text',
            ),
            'title' => array(
                'title' => $this->l('Category Title'),
                'width' => 60,
                'type' => 'text',
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false,
            ),
            'post_author' => array(
                'title' => $this->l('Author'),
                'width' => 60,
                'type' => 'text',
            ),
            'date_add' => array(
                'title' => $this->l('Created date'),
                'width' => 60,
                'type' => 'datetime',
            ),
        );
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        parent::__construct();
    }

    public function init()
    {
        parent::init();
        $this->_join = ' LEFT JOIN '
            . _DB_PREFIX_
            . 'blog_category_lang bcl ON bcl.id_blog_category = a.id_blog_category && bcl.id_lang ='
            . (int)Context::getContext()->cookie->id_lang
            . ' LEFT JOIN '
            . _DB_PREFIX_
            . 'blog_category_shop bcs ON a.id_blog_category = bcs.id_blog_category && bcs.id_shop IN('
            . implode(',',Shop::getContextListShopID())
            . ')';
        $this->_select = 'bcs.id_shop';
        $this->_select = 'bcl.title';
        $this->_select = 'a.post_position position';
        $this->_defaultOrderBy = 'position';
        $this->_defaultOrderWay = 'ASC';
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP) {
            $this->_group = 'GROUP BY a.id_blog_post';
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJqueryPlugin('select2');
        $this->addJs(_MODULE_DIR_.$this->module->name.'/views/js/blog_admin.js');
    }

    public function renderList()
    {
        if(isset($this->_filter) && trim($this->_filter) == '') {
            $this->_filter = $this->original_filter;
        }
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }

    public function renderForm()
    {
        $image_url = _MODULE_DIR_ . $this->module->name . '/views/img/no-img.jpg';
        if (!empty(Tools::getValue('id_blog_post'))) {
            $imageName = $this->module->getPostImage(Tools::getValue('id_blog_post'));
            $image_url = empty($imageName) ?
                $image_url :
                _MODULE_DIR_ . $this->module->name . '/views/img/images/' . $imageName['image_name_with_ext'];
        }
        $image = '<div id="image-show" class="col-lg-6"><img id="image-img" src="'
            . $image_url
            . '" class="img-thumbnail" width="100"></div>';

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Blog Post'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'post_title',
                    'lang' => true,
                    'required' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Category'),
                    'name' => 'id_blog_category',
                    'class' => 'chosen',
                    'col' => 5,
                    'required' => true,
                    'options' => array(
                        'query' => BlogCategory::getAllCategories(
                            implode(',', Shop::getContextListShopID()),
                            (int)Context::getContext()->cookie->id_lang
                        ),
                        'id' => 'id_blog_category',
                        'name' => 'title',
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Description'),
                    'name' => 'post_description',
                    'lang' => true,
                    'required' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Content'),
                    'name' => 'post_content',
                    'autoload_rte' => true,
                    'rows' => 5,
                    'cols' => 40,
                    'lang' => true,
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta title'),
                    'name' => 'post_meta_title',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta description'),
                    'name' => 'post_meta_description',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Friendly url'),
                    'name' => 'post_seo_url',
                    'lang' => true,
                    'required' => true,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Image above the post'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image
                ),
                array(
                    'type' => 'text',
                    'col' => 6,
                    'label' => $this->l('Author'),
                    'name' => 'post_author',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        $this->tpl_form_vars = array(
            'active' => $this->object->active,
            'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
        );

        return parent::renderForm();
    }

    public function initToolbar()
    {
        parent::initToolbar();
    }

    public function processPosition()
    {
        if($this->tabAccess['edit'] !== '1') {
            $this->errors[] = Tools::displayError('You do not have permission to edit this.');
        } elseif (!Validate::isLoadedObject(
            $object = new BlogPost(
                (int)Tools::getValue($this->identifier, Tools::getValue('id_blog_post', 1))
            )
        )
        ) {
            $this->errors[] = Tools::displayError(
                'An error occurred while updating the status for an object.'
                ) .' <b>'. $this->table.'</b> '.Tools::displayError('(cannot load object)');
        }

        if(!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position'))) {
            $this->errors[] = Tools::displayError('Failed to update the position.');
        } else {
            Tools::redirectAdmin(
                self::$currentIndex .'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&token='
                .Tools::getAdminTokenLite('AdminPrestaProsBlogPostController')
            );
        }
    }
    public function ajaxProcessUpdatePositions()
    {
        $id_blog_post = (int)(Tools::getValue('id'));
        $way = (int)(Tools::getValue('way'));
        $positions = Tools::getValue($this->table);
        if (is_array($positions)) {
            foreach ($positions as $key => $value) {
                $pos = explode('_', $value);
                if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_blog_post)) {
                    $position = $key + 1;
                    break;
                }
            }
        }
        $blogPostClass = new BlogPost($id_blog_post);
        if (Validate::isLoadedObject($blogPostClass)) {
            if (isset($position) && $blogPostClass->updatePosition($way, $position)){
                Hook::exec('action'.$this->className.'Update');
                die(true);
            } else {
                die('{"hasError" : true, errors : "Can not update blogPostClass position"}');
            }
        } else {
            die('{"hasError" : true, "errors" : "This blogPostClass can not be loaded"}');
        }
    }

    public function afterAdd($object)
    {
        foreach ($this->context->controller->getLanguages() as $language) {
            $object->post_seo_url[$language['id_lang']] = Tools::link_rewrite($object->post_seo_url[$language['id_lang']]);
        }
        $object->update();
        $this->setPostImage();

        return parent::afterAdd($object);
    }

    public function afterUpdate($object)
    {
        foreach ($this->context->controller->getLanguages() as $language) {
            $object->post_seo_url[$language['id_lang']] = Tools::link_rewrite($object->post_seo_url[$language['id_lang']]);
        }
        $object->update();

        $this->setPostImage();
        return parent::afterUpdate($object);
    }

    protected function setPostImage()
    {
        if ($fileNames = $this->uploadPostImage()) {
            $this->saveImage($fileNames, Tools::getValue('id_blog_post'));
        }
    }

    protected function saveImage($fileNames, $postId)
    {
        if (empty($fileNames)) {
            $this->errors[] = $this->l('An error occurred while saving an image to database');

            return false;
        }
        $imageData = $this->module->getPostImage($postId);
        $image = empty($imageData) ? new BlogPostImage() : new BlogPostImage((int)$imageData['id_blog_post_image']);
        $image->id_blog_post = $postId;
        $image->image_name_with_ext = $fileNames['image_name_with_ext'];
        $image->image_card_name_with_ext = $fileNames['image_card_name_with_ext'];
        $image->image_name = explode('.', $fileNames['image_name_with_ext'])[0];

        try {
            empty($imageData) ? $image->add() : $image->update();
            return  true;
        } catch (PrestaShopException $exception) {
            $this->errors[] = $exception->getMessage();
        }

        return  false;
    }

    protected function uploadPostImage()
    {
        if (isset($_FILES['image']) && isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            if ($this->checkImageError($_FILES['image']['error'])
                && $this->checkImageMemoryLimit($_FILES['image']['tmp_name'])
            ) {
                return $this->changeImageLocation($_FILES['image']['tmp_name'], $_FILES['image']['name']);
            }
        }
        if (!empty(Tools::getValue('id_blog_post'))
            && $file = $this->getFileByPostId(Tools::getValue('id_blog_post'))
        ) {
            return  $file;
        }

        return false;
    }

    protected function getFileByPostId($postId)
    {
        $flagsImageData = $this->module->getPostImage($postId);

        return !empty($flagsImageData) ? $flagsImageData : false;
    }

    protected function checkImageError($error)
    {
        if (!empty($error)) {
            $this->errors[] = $error;

            return false;
        }

        return true;
    }

    protected function checkImageMemoryLimit($savePath)
    {
        if (!ImageManager::checkImageMemoryLimit($savePath)) {
            $this->errors[] = $this->l('Memory Limit reached');

            return false;
        }

        return true;
    }

    protected function changeImageLocation($savePath, $name)
    {
        $cardName = 'card_' . $name;
        if (!ImageManager::resize(
                $savePath,
                _PS_MODULE_DIR_ . $this->module->name . '/views/img/images/' . $name,
                600,
                400
            )
        ) {
            $this->errors[] = $this->l('An error occurred during the image upload');
            return false;
        }
        copy(
            _PS_MODULE_DIR_ . $this->module->name . '/views/img/images/' . $name,
            _PS_MODULE_DIR_ . $this->module->name . '/views/img/images/' . $cardName
        );
        if (!ImageManager::resize(
            _PS_MODULE_DIR_ . $this->module->name . '/views/img/images/' . $cardName,
            _PS_MODULE_DIR_ . $this->module->name . '/views/img/images/' . $cardName,
            350,
            250
        )
        ) {
            $this->errors[] = $this->l('An error occurred during the image upload');
            return false;
        }

        return  ['image_name_with_ext' => $name, 'image_card_name_with_ext' => $cardName];
    }
}