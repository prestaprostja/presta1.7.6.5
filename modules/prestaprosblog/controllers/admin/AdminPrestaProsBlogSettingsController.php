<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

if (!defined('_PS_VERSION_')) {
    exit;
}


class AdminPrestaProsBlogSettingsController extends ModuleAdminController
{
    public function __construct()
    {
        $module_name = "prestaprosblog";
        Tools::redirectAdmin(
            'index.php?controller=AdminModules&configure=' . $module_name
            . '&token=' . Tools::getAdminTokenLite('AdminModules')
        );
    }
}
