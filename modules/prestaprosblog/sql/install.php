<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

$sql = array();

$sql[] = ' CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blog_category` (
        `id_blog_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `id_parent_category` int(10),
        `position` int(10) unsigned NOT NULL,
        PRIMARY KEY (`id_blog_category`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

$sql[] = ' CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blog_category_lang` (
        `id_blog_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `id_lang` int(10) unsigned NOT NULL,
        `title` varchar(64),
        `description` varchar(128),
        `meta_title` varchar(64),
        `meta_description` varchar(64),
        `seo_url` varchar(64),
        PRIMARY KEY (`id_blog_category`, `id_lang`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "blog_category_shop` (
	`id_blog_category` int(11) NOT NULL,
	`id_shop` int(11) unsigned NOT NULL,
	PRIMARY KEY (`id_blog_category`,`id_shop`)
)ENGINE=" . _MYSQL_ENGINE_ . " DEFAULT CHARSET=utf8";

$sql[] = ' CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blog_post` (
        `id_blog_post` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `id_blog_category` int(10) unsigned NOT NULL,
        `post_position` int(10) unsigned NOT NULL,
        `active`  int(1),
        `post_author`  varchar(64),
        `date_add` datetime NOT NULL,
        PRIMARY KEY (`id_blog_post`, `id_blog_category`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

$sql[] = ' CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blog_post_lang` (
        `id_blog_post` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `id_lang` int(10) unsigned NOT NULL,
        `post_title` varchar(64),
        `post_description` varchar(256),
        `post_content` TEXT,
        `post_meta_title` varchar(64),
        `post_meta_description` varchar(128),
        `post_seo_url` varchar(64),
        PRIMARY KEY (`id_blog_post`, `id_lang`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

$sql[] = ' CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blog_post_image` (
        `id_blog_post_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `id_blog_post` int(10) unsigned NOT NULL,
        `image_name` varchar(64),
        `image_name_with_ext` varchar(128),
        `image_card_name_with_ext` varchar(128),
        PRIMARY KEY (`id_blog_post_image`, `id_blog_post`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
