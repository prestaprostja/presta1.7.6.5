<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from PrestaProsBlog
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@prestapros.com
 *
 * @author    PrestaPros.com
 * @copyright Copyright (c) 2017-2020 PrestaPros
 * @license   Commercial license
 * @package   prestaprosblog
 */

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'blog_category`';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'blog_category_lang`';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'blog_category_shop`';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'blog_post`';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'blog_post_lang`';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'blog_post_image`';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
